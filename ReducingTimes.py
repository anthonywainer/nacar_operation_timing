import logging
import os
import io
from multiprocessing import Process
from multiprocessing.pool import Pool
from queue import Queue
from threading import Thread

import pandas as pd
# Threaded function for queue processing.
import time


def getFilasInTuplas(nomfile):
    print("INFO: Datos obtenidos de ", nomfile)

    f = io.open('logsTest/test/'+nomfile, 'r', encoding='latin-1')
    in_data = []

    for linea in f:
        if str(linea).find('#') > -1:
            t = list(linea.split('#', 2))
            if len(t) > 2:
                t[2] = t[2].replace('#', '')
                t = tuple(t)
            else:
                t = tuple(linea.split(' ', 2))
        else:
            t = tuple(linea.split(' ', 2))

        if len(t) >= 3:
            in_data.append(t)
    f.close()
    return in_data

def crawl(q, result):
    while not q.empty():
        work = q.get() #fetch new work from the Queue
        data =getFilasInTuplas(work[1])
        logging.info("Requested..." + work[1])
        result[work[0]] = data #Store data back at correct index

        #signal to the queue that task has been processed
        q.task_done()
    return True


if __name__ == '__main__':
    start=time.time()
    #os.path.walk('logsTest/test/', process_files_parallel, None)
    # set up the queue to hold all the urls
    q = Queue(maxsize=0)
    urls = os.listdir('logsTest/test/')
    # Use many threads (50 max, or one for each url)
    num_theads = min(50, len(urls))

    # Populating Queue with tasks
    results = [{} for x in urls]

    # load up the queue with the urls to fetch and the index for each job (as a tuple):
    for i in range(len(urls)):
        # need the index and the url in each queue item.
        q.put((i, urls[i]))

    # 1 thread
    start = time.time()
    for i in range(len(urls)):
        logging.debug('Starting thread ', i)

        worker = Process(target=getFilasInTuplas, args=(urls[i],))
        worker.start()  # setting threads as "daemon" allows main program to
        worker.join()
    print("MULPROCESS: ", time.time() - start)

    start = time.time()
    for i in range(num_theads):
        logging.debug('Starting thread ', i)
        worker = Thread(target=crawl, args=(q, results))
        worker.setDaemon(True)  # setting threads as "daemon" allows main program to
        # exit eventually even if these dont finish
        # correctly.
        worker.start()
    # now we wait until the queue has been processed
    q.join()
    print("Thread: ", time.time()-start)

    start = time.time()
    for i in range(len(urls)):
        data = getFilasInTuplas(urls[i])

    print("Normal: ", time.time() - start)


