# coding=utf-8
import pandas as pd
import utilitarios
from datetime import datetime as dt
from datetime import timedelta as td

'''
    Datos filtrados por palabras reservadas
'''
# Escenario predecesor -> operativa
codigos_flujos = [
    {
        'LABGVEB565B56501C': {
            'LABGVEB632B63201C': {  # deposito lectura de tarjeta
                "inicio": ['LABGVEB565B56501C', 'LABGVEB632B63201C'],
                "fin": ['LABGVEB632B63201C', 'LABGVEB565B56501C'],
            },
            'LABGVEB503B50301C': {  # retiro lectura de tarjeta
                "inicio": ['LABGVEB565B56501C', 'LAPEVEPE1MPE1M01C'],
                "fin": ['LABGVEB503B50301C', 'LABGVEB565B56501C'],
            },
        },

        'LAESVEES01ES0101C': {
            'LAPEVEPE28PE2801C': {  # operativa con DNI
                "inicio": ['LAESVEES01ES0101C', 'LAPEVEPE28PE2801C'],
                "fin": ['LAKTVEKTAFKTAF01C', 'LAESVEES01ES0101C'],
            },
            'LAMCVEMPOPMPOP01C': {  # pago de tarjeta de credito
                "inicio": ['LAESVEES01ES0101C', 'LAMCVEMP20MP2001C'],
                "fin": ['LAMCVEMP20MP2001C', 'LAESVEES01ES0101C'],
            },

            'LABGVEB565B56501C': {  # lectura de tarjeta
                "inicio": ['LAESVEES01ES0101C', 'LABGVEB565B56501C'],
                "fin": ['LABGVEB565B56501C', 'LAESVEES01ES0101C'],
            },

            'LABGVEB632B63201C': {  # deposito
                "inicio": ['LAESVEES01ES0101C', 'LABGVEB632B63201C'],
                "fin": ['LABGVEB632B63201C', 'LAESVEES01ES0101C']
            },
            'LABGVEB503B50301C': {  # retiro
                "inicio": ['LAESVEES01ES0101C', 'LAPEVEPE1MPE1M01C'],
                "fin": ['LABGVEB503B50301C', 'LAESVEES01ES0101C']
            },
            'LAKTVEKTAFKTAF01C': {
                "fin": ['LAKTVEKTAFKTAF01C', 'LAESVEES01ES0101C'],
            },  # caso especial

            'LARCVERC70RC7001C': {  # recaudos y pago de servicios
                "inicio": ['LAESVEES01ES0101C', 'LARCVERC75RC7501C'],
                "fin": ['LARCVERC75RC7501C', 'LAESVEES01ES0101C'],
            },

        },

        # 'LAESVEES03ES0301C': {
        #     'LARTVERT31RT3111C': {
        #         "inicio": ['LAESVEES03ES0301C', 'LARTVERT31RT3111C'],
        #         "fin": ['LARTVERT31RT3111C', 'LAESVEES03ES0301C'],
        #     }
        # },

    }

]


def getListFlujos(cod_oper):
    # Depositos básicos (Solo retiro, Solo deposito, etc): valor 0
    lista_flujos_deposito = {
        'Depósito con N° de Cuenta': [0, 'LABGVEB632B63201C'],
        'Depósito con N° de Cuenta + Tx Significativas': [1, 'LABGVEB632B63201C', 'LABGVEB852B85201C'],

        'Depósito con N° de Cuenta con mod. de datos': [2, 'LABGVEB632B63201C',
                                                        'LAPEVEPEWZPEWZ02C'],

        'Depósito con N° de Cuenta con mod. de datos + Tx Significativas': [3, 'LABGVEB632B63201C',
                                                                            'LABGVEB852B85201C',
                                                                            'LAPEVEPEWZPEWZ02C'],

        'Depósito Cheques': [4, 'LABGVEB632B63201C', 'LABGVEB632B63301C'],
        'Depósito Cheques + Tx Significativas': [5, 'LABGVEB632B63201C', 'LABGVEB632B63301C', 'LABGVEB852B85201C'],

        'Depósito Especial': [6, 'LABGVEB632B63201C', 'LABGVEB632B63201C', 'LABGVEB632B63204C'],
        'Depósito Especial + Tx Significativas': [7, 'LABGVEB632B63201C', 'LABGVEB632B63201C', 'LABGVEB632B63204C',
                                                  'LABGVEB852B85201C'],

        'Depósito Cheques Especial': [8, 'LABGVEB632B63201C', 'LABGVEB632B63301C', 'LABGVEB632B63204C'],
        'Depósito Cheques Especial + Tx Significativas': [9, 'LABGVEB632B63201C', 'LABGVEB632B63301C',
                                                          'LABGVEB632B63204C',
                                                          'LABGVEB852B85201C', ],

    }

    lista_flujos_retiro = {
        'Retiro con N° de Cuenta': [0, 'LABGVEB503B50301C'],
        'Retiro con N° de Cuenta + Tx Significativas': [1, 'LABGVEB503B50301C', 'LABGVEB852B85201C'],

    }
    lista_flujos_dni = {
        'Depósito con DNI': [0, 'LABGVEB632B63201C'],
        'Depósito con DNI + Tx Significativas': [2, 'LABGVEB632B63201C', 'LABGVEB852B85201C'],

        'Retiro con DNI': [1, 'LABGVEB503B50301C'],
        'Retiro con DNI + Tx Significativas': [3, 'LABGVEB503B50301C', 'LABGVEB852B85201C'],

        'Pago de Tarjeta con DNI': [4, 'LAMCVEMPOPMPOP01C'],
        'Pago de Tarjeta con DNI + Tx Significativas': [5, 'LAMCVEMPOPMPOP01C', 'LABGVEB852B85201C'],
    }

    lista_flujos_lec_tarjeta = {
        'Depósito con lectura de tarjeta': [0, 'LABGVEB632B63201C'],
        'Depósito con lectura de tarjeta + Tx Significativas': [2, 'LABGVEB632B63201C', 'LABGVEB852B85201C'],

        'Retiro con lectura de tarjeta': [1, 'LABGVEB503B50301C'],
        'Retiro con lectura de tarjeta + Tx Significativas': [3, 'LABGVEB503B50301C', 'LABGVEB852B85201C'],
    }

    lista_flujos_recaudos = {
        'Recaudos y pago de Servicios': [0, 'LARCVERC70RC7001C'],
        'Recaudos y pago de Servicios + Tx Significativas': [1, 'LARCVERC70RC7001C', 'LABGVEB852B85201C'],

        'Recaudo con Búsqueda Alfabética de Cliente': [1, 'LARCVERC70RC7001C', 'LARCVERC86RC8601C'],
        'Recaudo con Búsqueda Alfabética de Cliente + Tx Significativas': [2, 'LARCVERC70RC7001C', 'LARCVERC86RC8601C',
                                                                           'LABGVEB852B85201C'],
    }
    lista_flujos_pago_tarjeta_credito = {
        'Pago de tarjeta de credito': [0, 'LAMCVEMPOPMPOP01C'],
        'Pago de Tarjeta de credito + Tx Significativas': [1, 'LAMCVEMPOPMPOP01C', 'LABGVEB852B85201C'],
    }

    flujos = {'LABGVEB632B63201C': lista_flujos_deposito, 'LABGVEB503B50301C': lista_flujos_retiro,
              'LAPEVEPE28PE2801C': lista_flujos_dni, 'LABGVEB565B56501C': lista_flujos_lec_tarjeta,
              'LARCVERC70RC7001C': lista_flujos_recaudos, 'LAMCVEMPOPMPOP01C': lista_flujos_pago_tarjeta_credito
              }

    if flujos.get(cod_oper):
        return flujos[cod_oper]
    else:
        return {}


def getListCodFlujos(cod_operativa):
    lista_cod_flujos = {
        'LABGVEB632B63201C': [  # deposito
            'LAESVEES01ES0101C',  # escenario
            'LABGVEB632B63201C',  # deposito
            'LAPEVEPE1MPE1M01C',  # consulta de solicitud especial
            'LABGVEB632B63204C',  # Deposito Especial
            'LABGVEB632B63301C',  # depósito de Cheques
            'LABGVEB632B63302C',  # Lineas a Pantalla
            'LABGVEB852B85201C',  # Tx. Sig
            'LAQGVEQGA4QGA401C',  # ventana de ayuda
            'LAISVEI007I00701C',  # Ventana de alta de seguros
            'LAPEVEPE28PE2801C',  # Ventana de DNI
            'LAPEVEPEWZPEWZ02C',  # Modificación de Datos de Contacto
            'LABGVEB632B63208C',  # Ventana Deposito 8c
            'LAPEVEBIOMBIOM01C',  # Biometrico 1
            'LAPEVEBIOMBIOM02C',  # Biometrico 2
            'LAPEVEBIOMBIOM03C',  # Biometrico 3
            'LAPEVEBIOMBIOM04C',  # Biometrico 4
            'LAPEVEBIOMBIOM05C',  # Biometrico 5
            'LAQGVEQG00QG0001C',  # Caja de Autorización

            'BGE0002',
        ],
        'LABGVEB503B50301C': [  # Retiro
            'LAESVEES01ES0101C',  # escenario
            'LABGVEB503B50301C',  # retiro
            'LAPEVEPE1MPE1M01C',  # consulta de solicitud especial
            'LAQGVEQG00QG0001C',  # Caja de Autorización
            'LABGVEB852B85201C',  # Tx. Sig
            'LABGVEB427B42701C',  # Ventana Consulta de Partícipes Cuenta
            'LAPEVEBIOMBIOM01C',  # Biometrico 1
            'LAPEVEBIOMBIOM02C',  # Biometrico 2
            'LAPEVEBIOMBIOM03C',  # Biometrico 3
            'LAPEVEBIOMBIOM04C',  # Biometrico 4
            'LAPEVEBIOMBIOM05C',  # Biometrico 5
            'LABGVEB632B63302C',  # Lineas a Pantalla

            'LAPEVEPEWZPEWZ02C',  # Modificación de Datos de Contacto
            'LAPEVEPE28PE2801C',  # Ventana de DNI
            'LASMVESMNNSMNN01C',  # Ventana de datos de ventas

            'LAFIVEIMVIIMWVIFI',  # Chequeo de firma
            'LAFIVEIMVIIMWPODE',  # Chequeo de firma
            'LAFIVEVIFIVIFI01C',  # Chequeo de firma
            'LAFIVEVIFIVIFI02C',  # Chequeo de firma

            'LAISVEI007I00701C',  # Ventana de alta de seguros
            'LABGVEB504B50401C',  # PAGO DE INTERESES POR CAJA
            'LATCVETC05TC0501C',  # Ventana de tabla corporativa
            'LABGVEB047B04701C',  # Ventana Apertura rápida - buscar
            'LABGVEB047B04702C',  # Ventana Apertura rápida - buscar
            'LAPEVEPE49PE4901C',  # Ventana Consulta SBS
            'LAUGVEU882U88201C',  # Ventana simulación financiera por cuenta

            'LAPEVEPE33PE3301C',  # Ventana consulta documentos de identidad
            'LAMCVEMPRAMPRA01C',  # Ventana Pasajero frecuente LIFEMILES
            'LAPEVEPERBPERB01C',  # Ventana consulta de scoring online
            'LAESVEES03ES0301C',  # Ventana de listado de tareas
            'LAESVEES04ES0401C',  # Ventana de mantenimiento tarea
            'LAQGVEQGA4QGA401C',  # ventana de ayuda

            'LAMCVEMC65MC6501C',  # Ventana para Generar / Cambiar clave
            'LAMCVEMPILMPIL01C',  # Ventana de Partícipes de una cuenta

            'LABGVEAA01AA0102C',  # Ventana sin detalle, por registrar
            'LABGVEB503B63302C',  # Ventana sin detalle, por registrar

            'BGE0002',
        ],
        'LAPEVEPE28PE2801C': [  # Operatoria con dni
            'LAESVEES01ES0101C',  # escenario
            'LAPEVEPE28PE2801C',  # Ventana de DNI
            'LABGVEB632B63201C',  # Depósito
            'LABGVEB632B63204C',  # Deposito Especial
            'LABGVEB632B63208C',  # Ventana Deposito 8c
            'LAPEVEPE1MPE1M01C',  # consulta de solicitud especial
            'LAPEVEBIOMBIOM01C',  # Biometrico 1
            'LAPEVEBIOMBIOM02C',  # Biometrico 2
            'LAPEVEBIOMBIOM03C',  # Biometrico 3
            'LAPEVEBIOMBIOM04C',  # Biometrico 4
            'LAPEVEBIOMBIOM05C',  # Biometrico 5
            'LABGVEB503B50301C',  # Retiro
            'LABGVEB852B85201C',  # Tx. Sig
            'LASMVESMNNSMNN01C',  # Ventana de datos de ventas
            'LAISVEI007I00701C',  # Ventana de alta de seguros
            'LAQGVEQG00QG0001C',  # Caja de Autorización
            'LABGVEB632B63302C',  # Lineas a Pantalla
            'LAQGVEQGA4QGA401C',  # ventana de ayuda

            'LABGVEB565B56501C',  # Ventana de lectura tarjeta - cuentas
            'LAPEVEPEWZPEWZ02C',  # Modificación de Datos de Contacto
            'LABGVEB427B42701C',  # Ventana Consulta de Partícipes Cuenta
            'LABGVEB401B40101C',  # CONSULTA DE MOVIMIENTOS
            'LAUGVEU402U40201C',  # Consulta deuda pendiente
            'LAUGVEU402U40202C',  # Consulta deuda pendiente
            'LAISVEI507I50701C',  # PAGO DE RECIBOS SEGUROS

            'LABGVEB632B63301C',  # Depósito de Cheques

            'LAUGVEU780U78001C',  # Ventana de adelante de cuotas
            'LAESVEES04ES0401C',  # Ventana de mantenimiento tarea

            'LABGVEB504B50401C',  # PAGO DE INTERESES POR CAJA

            'LAFIVEIMVIIMWVIFI',  # Chequeo de firma
            'LAFIVEIMVIIMWPODE',  # Chequeo de firma
            'LAFIVEVIFIVIFI01C',  # Chequeo de firma
            'LAFIVEVIFIVIFI02C',  # Chequeo de firma

            'LAUGVEU501U50101C',  # ventana Cancelación Anticipada
            'LAUGVEU507U50701C',  # ventana de pago de Recibos

            'LAQGVEQG50QG5001C',  # ventana Ingreso / Salida Cajeros
            'LAHTVEHT08HT0801C',  # ventana de Administración
            'LAHTVEHT70HT7001C',  # ventana Ingresos sobrantes cajero
            'LAQGVEQG66QG6601C',  # ventana consulta diario operaciones
            'LAQGVEQG66QG6602C',  # ventana consulta diario operaciones
            'LAISVEI507I50720C',  # Ventana Pago de recibos seguros

            'LATCVETC05TC0501C',  # Ventana de tabla corporativa

            'LABGVEB047B04701C',  # Ventana Apertura rápida - buscar
            'LABGVEB047B04702C',  # Ventana Apertura rápida - buscar
            'LAPEVEPE49PE4901C',  # Ventana Consulta SBS
            'LAUGVEU882U88201C',  # Ventana simulación financiera por cuenta
            'LAPEVEPE33PE3301C',  # Ventana consulta documentos de identidad
            'LAMCVEMPRAMPRA01C',  # Ventana Pasajero frecuente LIFEMILES
            'LAPEVEPERBPERB01C',  # Ventana consulta de scoring online

            'LABGVEAA01AA0102C',  # Ventana sin detalle, por registrar

            'LAESVEES03ES0301C',  # Ventana de listado de tareas
            'LAMCVEMC65MC6501C',  # Ventana para Generar / Cambiar clave
            'LAMCVEMPILMPIL01C',  # Ventana de Partícipes de una cuenta

            'LABGVEB503B63302C',  # Ventana sin detalle, por registrar
            'LAGPVEGA11GA1101C',  # Ventana sin detalle, por registrar
            'LABGVEB521B52101C',  # Ventana sin detalle, por registrar

            'LAMCVEMP69MP6901C',  # Consulta rápida de tarjetas
            'LAMCVEMPOPMPOP01C',  # Opciones de pago a tarjeta de Crédito
            'LAMCVEMP20MP2001C',  # Anulación de Pagos a cuenta
            'LAQGVEQG95QG9501C',  # Traspaso entre cajeros
            'LAQGVEQGPAQGPA01C',  # Ventana sin detalle

            'BGE0002'
        ],
        'LABGVEB565B56501C': [  # lectura de tarjeta
            'LAESVEES01ES0101C',  # Escenario
            'LABGVEB565B56501C',  # lectura de tarjeta
            'LABGVEB632B63201C',  # Depósito
            'LABGVEB632B63208C',  # Ventana Deposito 8c
            'LABGVEB632B63204C',  # Deposito Especial
            'LAPEVEPE1MPE1M01C',  # consulta de solicitud especial
            'LAPEVEBIOMBIOM01C',  # Biometrico 1
            'LAPEVEBIOMBIOM02C',  # Biometrico 2
            'LAPEVEBIOMBIOM03C',  # biometrico 3
            'LAPEVEBIOMBIOM04C',  # biometrico 4
            'LAPEVEBIOMBIOM05C',  # biometrico 5
            'LABGVEB503B50301C',  # Retiro
            'LABGVEB852B85201C',  # Tx. Sig
            'LASMVESMNNSMNN01C',  # Ventana de datos de ventas
            'LAISVEI007I00701C',  # Ventana de alta de seguros
            'LABGVEB427B42701C',  # consulta de participes cuenta
            'LABGVEB632B63302C',  # Lineas a Pantalla
            'LAQGVEQGA4QGA401C',  # ventana de ayuda
            'LAPEVEPE28PE2801C',  # Ventana de DNI
            'LAPEVEPEWZPEWZ02C',  # modificación de datos
            'LAQGVEQG00QG0001C',  # Caja de Autorización

            'LABGVEB632B63301C',  # depósito de Cheques

            'LABGVEB504B50401C',  # PAGO DE INTERESES POR CAJA

            'LAESVEES04ES0401C',  # Ventana de mantenimiento tarea

            'LAFIVEIMVIIMWVIFI',  # Chequeo de firma
            'LAFIVEIMVIIMWPODE',  # Chequeo de firma
            'LAFIVEVIFIVIFI01C',  # Chequeo de firma
            'LAFIVEVIFIVIFI02C',  # Chequeo de firma

            'LATCVETC05TC0501C',  # Ventana de tabla corporativa
            'LAISVEI507I50720C',  # Ventana Pago de recibos seguros
            'LABGVEB047B04701C',  # Ventana Apertura rápida - buscar
            'LABGVEB047B04702C',  # Ventana Apertura rápida - buscar
            'LAPEVEPE49PE4901C',  # Ventana Consulta SBS
            'LAUGVEU882U88201C',  # Ventana simulación financiera por cuenta
            'LAPEVEPE33PE3301C',  # Ventana consulta documentos de identidad
            'LAMCVEMPRAMPRA01C',  # Ventana Pasajero frecuente LIFEMILES
            'LAPEVEPERBPERB01C',  # Ventana consulta de scoring online

            'LABGVEAA01AA0102C',  # Ventana sin detalle, por registrar

            'LAESVEES03ES0301C',  # Ventana de listado de tareas
            'LAMCVEMC65MC6501C',  # Ventana para Generar / Cambiar clave
            'LAMCVEMPILMPIL01C',  # Ventana de Partícipes de una cuenta
            'LABGVEB503B63302C',  # Ventana sin detalle, por registrar

            'BGE0002',
        ],
        # recaudos
        'LARCVERC70RC7001C': [
            'LAESVEES01ES0101C',  # Escenario
            'LARCVERC70RC7001C',  # ventana de recaudos
            'LARCVERC75RC7501C',  # consulta de recaudos
            'LARCVERC72RC7201C',  # campo de referencia
            'LARCVERC76RC7601C',  # consulta cliente
            'LARCVERC70RC7002C',  # consulta de recaudos
            'LABGVEB852B85201C',  # Tx. Sig
            'LARCVERC86RC8601C',  # Consulta Alfabética de Clientes
            'LABGVEB904B90401C',  # LISTADO DE TABLAS
            'LAQGVEQG62QG6201C',  # CONSULTA DE DATOS DE UN TERMINAL
            'LABGVEB632B63302C',  # Lineas a Pantalla
            'LAPEVEBIOMBIOM01C',  # Biometrico 1
            'LAPEVEBIOMBIOM02C',  # Biometrico 2
            'LAPEVEBIOMBIOM03C',  # biometrico 3
            'LAPEVEBIOMBIOM04C',  # biometrico 4
            'LAPEVEBIOMBIOM05C',  # biometrico 5
            'LAQGVEQGA4QGA401C',  # ventana de ayuda
            'LAQGVEQG00QG0001C',  # Caja de Autorización
            'LARCVERC70RC7003C',  # ventana de recaudos
            'LAESVEES05ES0501C',  # Escenario de Tablas Corporativas

            'LARCVERC27RC2701C',  # Ventana por registrar
            'LARCVERC70RC7004C',  # Ventana por registrar
            'LAGPVEGA10GA1001C',  # Ventana por registrar
            'LATCVETC84TC8401C',  # Ventana por registrar

            'BGE0002',
        ],
        'LAMCVEMPOPMPOP01C': [  # pago de tarjeta de crédito
            'LAESVEES01ES0101C',  # escenario
            'LAMCVEMPOPMPOP01C',  # Opciones de pago a tarjeta de Crédito
            'LAMCVEMP20MP2001C',  # Anulación de Pagos a cuenta
            'LAPEVEPE28PE2801C',  # Relaciones con el banco
            'LAMCVEMP69MP6901C',  # Consulta rápida de tarjetas
            'LABGVEB852B85201C',  # Tx. Sig
        ]
    }

    if lista_cod_flujos.get(cod_operativa):
        return lista_cod_flujos[cod_operativa]
    else:
        return []


def iniDetailCod_Tx_Ventana():
    lista_detalle_codigos = {
        'LAESVEES01ES0101C': {
            'Detalle': 'Escenario de Operatoria de Cajeros'
        },
        # - VENTANAS - #

        # Deposito
        'LABGVEB632B63201C': {
            'Detalle': 'Depósito'
        },
        'LABGVEB852B85201C': {
            'Detalle': 'Registro de Tx Significativas'
        },
        'LABGVEB632B63204C': {
            'Detalle': 'Deposito Especial, Información Adicional'
        },
        'LAPEVEPEWZPEWZ02C': {
            'Detalle': 'Modificación de Datos de Contacto'
        },
        'LAPEVEPE1MPE1M01C': {
            'Detalle': 'Consulta de solicitud especial'
        },
        'LAQGVEQG00QG0001C': {
            'Detalle': 'Caja de Autorización'
        },
        'LARCVERC72RC7201C': {
            'Detalle': 'Campo Referencia'
        },
        'LABGVEB632B63301C': {
            'Detalle': 'Deposito de Cheques'
        },
        'LABGVEB632B63302C': {
            'Detalle': 'Lineas a Pantalla'
        },
        # nuevas
        'LAQGVEQGA4QGA401C': {
            'Detalle': 'ventana de ayuda'
        },
        'LAPEVEPE28PE2801C': {
            'Detalle': 'Operatoria con DNI / Relaciones con el Banco'
        },
        'LAPEVEBIOMBIOM03C': {
            'Detalle': 'Biométrico 3'
        },
        'LAPEVEBIOMBIOM04C': {
            'Detalle': 'Biométrico 4'
        },
        'LAPEVEBIOMBIOM05C': {
            'Detalle': 'Biométrico 5'
        },
        'LAKTVEKTAFKTAF01C': {
            'Detalle': 'Resolutor'
        },
        'LABGVEB632B63208C': {
            'Detalle': 'Ventana Deposito'
        },

        # Retiro
        'LABGVEB503B50301C': {
            'Detalle': 'Retiro'
        },
        'LAQGVEQG30QG3001C': {
            'Detalle': 'Ventana de Firma de Autorización'
        },
        'LAISVEI007I00701C': {
            'Detalle': 'Ventana de alta de seguros'
        },
        'LASMVESMNNSMNN01C': {
            'Detalle': 'Ventana de datos de ventas'
        },
        'LABGVEB427B42701C': {
            'Detalle': 'Ventana Consulta de Partícipes Cuenta'
        },
        # lectura de tarjetas
        'LABGVEB565B56501C': {
            'Detalle': 'Ventana de lectura tarjeta - cuentas'
        },
        'LARCVERC70RC7001C': {
            'Detalle': 'Recaudos / Facturas'
        },
        'LARCVERC75RC7501C': {
            'Detalle': 'Consulta Recaudos y/o Convenios'
        },
        'LARCVERC86RC8601C': {
            'Detalle': 'Consulta Albabetica de Cliente'
        },
        'LARCVERC76RC7601C': {
            'Detalle': 'Consulta de cliente'
        },
        'LARCVERC70RC7002C': {
            'Detalle': 'Consulta de Recaudos'
        },
        'LARCVERC70RC7003C': {
            'Detalle': 'Ventana de Recaudos'
        },
        'LABGVEB401B40101C': {
            'Detalle': 'CONSULTA DE MOVIMIENTOS'
        },
        'LABGVEB904B90401C': {
            'Detalle': 'LISTADO DE TABLAS'
        },
        'LAQGVEQG62QG6201C': {
            'Detalle': 'CONSULTA DE DATOS DE UN TERMINAL'
        },
        'LAUGVEU402U40201C': {
            'Detalle': 'Consulta deuda pendiente'
        },
        'LAUGVEU402U40202C': {
            'Detalle': 'Consulta deuda pendiente'
        },
        'LAISVEI507I50701C': {
            'Detalle': 'PAGO DE RECIBOS SEGUROS'
        },
        'LABGVEB504B50401C': {
            'Detalle': 'PAGO DE INTERESES POR CAJA'
        },
        'LAFIVEIMVIIMWVIFI': {
            'Detalle': 'Chequeo de firma'
        },
        'LAFIVEIMVIIMWPODE': {
            'Detalle': 'Chequeo de firma'
        },
        'LAFIVEVIFIVIFI01C': {
            'Detalle': 'Chequeo de firma'
        },
        'LATCVETC05TC0501C': {
            'Detalle': 'Ventana de tabla corporativa'
        },
        'LABGVEB047B04701C': {
            'Detalle': 'Ventana Apertura rápida - buscar'
        },
        'LABGVEB047B04702C': {
            'Detalle': 'Ventana Apertura rápida - buscar'
        },
        'LAPEVEPE49PE4901C': {
            'Detalle': 'Ventana Consulta SBS'
        },
        'LAUGVEU882U88201C': {
            'Detalle': 'Ventana simulación financiera por cuenta'
        },
        'LAPEVEPE33PE3301C': {
            'Detalle': 'Ventana consulta documentos de identidad'
        },
        'LAMCVEMPRAMPRA01C': {
            'Detalle': 'Ventana Pasajero frecuente LIFEMILES'
        },
        'LAPEVEPERBPERB01C': {
            'Detalle': 'Ventana consulta de scoring online'
        },
        'LAESVEES03ES0301C': {
            'Detalle': 'Ventana de listado de tareas'
        },
        'LAESVEES04ES0401C': {
            'Detalle': 'Ventana de mantenimiento tarea'
        },
        'LAMCVEMC65MC6501C': {
            'Detalle': 'Ventana para Generar / Cambiar clave'
        },
        'LAMCVEMPILMPIL01C': {
            'Detalle': 'Ventana de Partícipes de una cuenta'
        },
        'LAUGVEU780U78001C': {
            'Detalle': 'Ventana de adelante de cuotas'
        },
        'LAUGVEU501U50101C': {
            'Detalle': 'ventana Cancelación Anticipada'
        },
        'LAUGVEU507U50701C': {
            'Detalle': 'ventana De pago de Recibos'
        },
        'LAQGVEQG50QG5001C': {
            'Detalle': 'ventana Ingreso / Salida Cajeros'
        },
        'LAHTVEHT08HT0801C': {
            'Detalle': 'ventana de Administración'
        },
        'LAHTVEHT70HT7001C': {
            'Detalle': 'ventana Ingresos sobrantes cajero'
        },
        'LAQGVEQG66QG6601C': {
            'Detalle': 'ventana consulta diario operaciones'
        },
        'LAQGVEQG66QG6602C': {
            'Detalle': 'ventana consulta diario operaciones'
        },
        'LAISVEI507I50720C': {
            'Detalle': 'Ventana Pago de recibos seguros'
        },
        'LAESVEES05ES0501C': {
            'Detalle': 'Escenario de Tablas Corporativas'
        },

        'LAMCVEMP69MP6901C': {
            'Detalle': 'Consulta rápida de tarjetas'
        },
        'LAMCVEMPOPMPOP01C': {
            'Detalle': 'Opciones de pago a tarjeta de Crédito'
        },
        'LAMCVEMP20MP2001C': {
            'Detalle': 'Anulación de Pagos a cuenta'
        },
        'LAQGVEQG95QG9501C': {
            'Detalle': 'Traspaso entre cajeros'
        },
        'LAQGVEQGPAQGPA01C': {
            'Detalle': 'Sin detalle'
        },

        ###TRANSACCIONES
        # Deposito
        'PE1M': {
            'Detalle': 'Consulta de solicitud especial'
        },
        'B525': {
            'Detalle': 'Verificacion de cta'
        },
        'B426': {
            'Detalle': 'Consulta de autorizacion biometria'
        },
        'B633': {
            'Detalle': 'Operacion de deposito'
        },
        'B850': {
            'Detalle': 'Transacciones significativas'
        },
        'B851': {
            'Detalle': 'Transacciones significativas'
        },
        'B853': {
            'Detalle': 'Tipo de cambio para lavado de dinero'
        },
        'RC43': {
            'Detalle': 'Comisión por convenios'
        },
        'RC65': {
            'Detalle': 'Cuenta deposito especial/ Cuenta recaudadora'
        },
        'RC70': {
            'Detalle': 'Transacción de Recaudos'
        },
        'RC81': {
            'Detalle': 'Busqueda de convenios Recaudos'
        },
        'RC83': {
            'Detalle': 'Campo de referencia'
        },
        'RC84': {
            'Detalle': 'Validación de Rutinas'
        },
        'RC85': {
            'Detalle': 'Consulta de convenios'
        },
        'U0CD': {
            'Detalle': 'Control de deposito'
        },
        'MP69': {
            'Detalle': 'LISTADO TARJETAS A PARTIR CONTRATO'
        },
        'MP20': {
            'Detalle': 'PAGO DE TARJETA CREDITO'
        },
        'MP6G': {
            'Detalle': 'ESTADOS DE CUENTA'
        },
        'QG95': {
            'Detalle': 'BALANCIN - TRASPASO ENTRE CAJEROS'
        },

        # nuevo
        'PE27': {
            'Detalle': 'Localización de clientes'
        },
        'PE28': {
            'Detalle': 'Consulta relaciones con la entidad'
        },
        'PE1L': {
            'Detalle': 'Consulta segmento cliente'
        },
        'PE20': {
            'Detalle': 'Consulta de partícipes'
        },
        'PEW1': {
            'Detalle': 'Actualización de datos básicos'
        },
        'PEZ3': {
            'Detalle': 'Consulta de cliente no deseado'
        },
        'KTAF': {
            'Detalle': 'Resolutor'
        },
        'TC05': {
            'Detalle': 'Tablas corporativas'
        },
        'B503': {
            'Detalle': 'REINTEGRO/ TRASPASO - retiro'
        },
        'B504': {
            'Detalle': 'PAGO DE INTERESES POR CAJA'
        },

        'QG00': {
            'Detalle': 'Autorización de Operaciones'
        },
        'MP1E': {
            'Detalle': 'Consulta de Tipo de Carpetas'
        },
        'B852': {
            'Detalle': 'Limite de lavados de Dinero'
        },
        'TC03': {
            'Detalle': 'Consulta de datos de tablas'
        },
        'I007': {
            'Detalle': 'Alta de retiro de seguros'
        },
        'SMNN': {
            'Detalle': 'Pantalla ext. Nacar nuevos campos'
        },
        'QG30': {
            'Detalle': 'Inicio de sesión de terminal - firma'
        },
        'QG91': {
            'Detalle': 'Validación de saldo de Bóveda'
        },
        'PE2E': {
            'Detalle': 'Campaña actualización base de datos'
        },
        'MP1T': {
            'Detalle': 'Relación tarjetación base de datos'
        },
        'B565': {
            'Detalle': 'Consulta de cuentas activas'
        },
        'QGE2': {
            'Detalle': 'Valida centros autoriza de encuesta'
        },
        'MP00': {
            'Detalle': 'Menú de tarjetas'
        },
        'RC86': {
            'Detalle': 'TX. ALFABETICA DE CLIENTES'
        },
        'RCRO': {
            'Detalle': 'CONSULTA ENLACE EN RECAUDO'
        },
        'OY17': {
            'Detalle': 'INTEGRIDAD CREACION Y EXTORNO'
        },

        'U402': {
            'Detalle': 'Tx. Consulta deuda pendiente'
        },
        'I507': {
            'Detalle': 'Tx. PAGO DE RECIBOS SEGUROS'
        },
        'B401': {
            'Detalle': 'Tx. CONSULTA DE MOVIMIENTOS'
        },

        'B904': {
            'Detalle': 'Tx. LISTADO DE TABLAS'
        },
        'QG62': {
            'Detalle': 'Tx. CONSULTA DE DATOS DE UN TERMINAL'
        },
        'PEW2': {
            'Detalle': 'REPORTE ACTUALIZACION DE DATOS'
        },
        'PEWZ': {
            'Detalle': 'MODIFICACION DE CONTACTO'
        },
        'B427': {
            'Detalle': 'CONSULTA PARTICIPES CUENTA'
        },
        'MC61': {
            'Detalle': 'Validación de PIN de T. Financiero'
        },

        # errores
        'BGE0002': {
            "Detalle": "Cuenta no existe, Fin de Flujo"
        }

    }

    return lista_detalle_codigos


def buscarInicioFin(data_codigos, tmp_arriba, tmp_abajo):
    lista_ini = []
    lista_despro_up = []
    lista_fin = []
    lista_trazafin = []

    for f in range(0, len(data_codigos.axes[0])):
        # Obtener submatriz
        buscar = data_codigos.loc[f, 'index']

        # Opc desprotegiendo up
        t_despro_up = tmp_abajo[tmp_abajo['index'] < buscar]
        despro_up = t_despro_up.reset_index(drop=True)
        # protegiendo up
        t_arriba = tmp_arriba[tmp_arriba['index'] < buscar]
        arriba = t_arriba.reset_index(drop=True)
        # desprotegiendo down
        t_abajo = tmp_abajo[tmp_abajo['index'] > buscar]
        abajo = t_abajo.reset_index(drop=True)

        if arriba.size == 0:
            lista_ini.append("NA")
        else:
            lista_ini.append(arriba.iloc[-1, 2])

        if abajo.size == 0:
            lista_fin.append("NA")
        else:
            lista_fin.append(abajo.iloc[0, 2])

        if despro_up.size == 0:
            lista_despro_up.append("NA")
            lista_trazafin.append("NA")
        else:
            lista_despro_up.append(despro_up.iloc[-1, 2])
            lista_trazafin.append(despro_up.iloc[-1, 0])

    data_codigos['Up Protegiendo'] = lista_ini
    data_codigos['Up Desprotegiendo'] = lista_despro_up
    data_codigos['Down Desprotegiendo'] = lista_fin
    data_codigos['index_fin'] = lista_trazafin

    return data_codigos


def getDataxFiltro(data_hoy, palabras_reservadas):
    result = pd.DataFrame()
    codigos = []

    for i in range(len(palabras_reservadas)):
        tmp = data_hoy[
            data_hoy['6. Detalle'].str.contains(palabras_reservadas[i]) | data_hoy['6. Detalle'].str.contains(
                "BGE0002") | data_hoy['6. Detalle'].str.contains("4US:") |
            data_hoy['6. Detalle'].str.contains("4TE:")]
        tmp_codigo = tmp['6. Detalle'].values.tolist()

        for a in tmp_codigo:
            if "4US:" in a:
                codigos.append(a)
                continue
            if "4TE:" in a:
                codigos.append(a)
                continue

            if "BGE0002" in a:
                codigos.append("BGE0002")
            else:
                codigos.append(a.split()[12].upper())
        result = result.append(tmp, sort=True)

    result['3. Codigo'] = codigos

    result = result.reset_index()
    result = result.sort_values(by=['2. Hora', 'index'])

    return result.reset_index(drop=True)


def getDataxFiltroTx(data_hoy, palabras_reservadas):
    result = pd.DataFrame()
    codigos = []

    for i in range(len(palabras_reservadas)):

        tmp = data_hoy[data_hoy['6. Detalle'].str.contains(palabras_reservadas[i])]

        tmp_codigo = tmp['6. Detalle'].values.tolist()

        # Valen pos: 2, 5 (cod tx)
        for a in tmp_codigo:
            if a.find("AWT-EventQueue-0") > -1:
                codigos.append("Tx-" + str(a.split()[4]))
            else:
                codigos.append("Tx-" + str(a.split()[5]))

        result = result.append(tmp, sort=True)

    result['3. Codigo'] = codigos
    result['Up Protegiendo'] = '-'
    result['Up Desprotegiendo'] = '-'
    result['Down Desprotegiendo'] = '-'

    result = result.reset_index()
    result = result.sort_values(by=['2. Hora', 'index'])

    return result.reset_index(drop=True)


def getDataxFiltro2(data_hoy, palabras_reservadas):
    result = pd.DataFrame()
    codigos = []

    for i in range(len(palabras_reservadas)):
        tmp = data_hoy[data_hoy['6. Detalle'].str.contains(palabras_reservadas[i])]

        for j in range(0, len(tmp.axes[0])):
            codigos.append(palabras_reservadas[i].split()[-1])
        result = result.append(tmp, sort=True)

    result['3. Codigo'] = codigos

    result = result.reset_index()
    result = result.sort_values(by=['2. Hora', 'index'])

    lista_up = ["-" for a in range(len(result.axes[0]))]
    result['Up Protegiendo'] = lista_up
    result['Up Desprotegiendo'] = lista_up
    result['Down Desprotegiendo'] = lista_up

    return result.reset_index(drop=True)


'''
    OBTENIENDO DATA A UTILIZAR (Operaciones/Filtros)
'''


def getDatosxFecha(fecha, palabras_reservadas, operad):
    # Obtener datos de hoy de lista de logs

    data_hoy, li_ofi = utilitarios.getMatrizDatosHoy(fecha, operad)

    # Data filtrada, por palabra reservada "Se va al conservar el contexto de la ventana destino"
    # Se elimina los errores
    fil_data_codigos = getDataxFiltro(data_hoy, palabras_reservadas[0:1])

    # Data filtrada, por palabra reservada "Protegiendo"
    fil_data_reservada_up = getDataxFiltro2(data_hoy, ['Protegiendo'])

    # Data filtrada, por palabra reservada "Desprotegiendo"
    fil_data_reservada_down = getDataxFiltro2(data_hoy, ['Desprotegiendo'])

    # Data filtrada, por palabra reservada "Ejecutando transacci"
    fil_data_transactions = getDataxFiltroTx(data_hoy, palabras_reservadas[1:2])

    # Recorrer Codigos, busqueda hacia arriba - abajo, obtener códigos de ventana
    data_inifin = buscarInicioFin(fil_data_codigos, fil_data_reservada_up, fil_data_reservada_down)

    result_data = agruparDatos(data_inifin, fil_data_reservada_up, fil_data_reservada_down, fil_data_transactions)
    return result_data[['index', '1. Fecha', '2. Hora', '3. Codigo',
                        'Up Protegiendo', 'Up Desprotegiendo', 'Down Desprotegiendo',
                        '6. Detalle', 'index_fin']], li_ofi


def agruparDatos(m_codigos, m_proteg, m_desproteg, m_tx):
    result = pd.DataFrame()
    result = result.append(m_codigos, sort=True)
    result = result.append(m_proteg, sort=True)
    result = result.append(m_desproteg, sort=True)
    result = result.append(m_tx, sort=True)

    result = result.sort_values(by=['index'])

    return result.reset_index(drop=True)


def findPatronIni(df):
    try:
        if (df['Prev Cod.'] == codigos_flujos[0][df['Prev Cod.']][df['3. Codigo']]['inicio'][0]) and (
                df['Next Cod.'] == codigos_flujos[0][df['Prev Cod.']][df['3. Codigo']]['inicio'][1]):
            return True
        else:
            return False

    except KeyError:
        return False


def findPatronFin(df):
    try:
        # (tmp_data['3. Codigo'].str.upper() == patron_n[1]) & (tmp_data['Next Cod.'] == patron_n[0])
        '''if (df['3. Codigo'] == codigos_flujos[0][df['Next Cod.']][df['3. Codigo']]['fin'][0]) and (
                df['Next Cod.'] == codigos_flujos[0][df['Next Cod.']][df['3. Codigo']]['fin'][1]):'''

        if (df['Prev Cod.'] == codigos_flujos[0][df['Next Cod.']][df['3. Codigo']]['fin'][0]) and (
                df['Next Cod.'] == codigos_flujos[0][df['Next Cod.']][df['3. Codigo']]['fin'][1]):
            return True
        else:
            return False

    except KeyError:
        return False


def getDatosxPatron(matriz, puesto, list_cod_especiales, palabras_reservadas):
    if matriz.size < 1:
        print("NOTIFICACION: No se ha encontrado datos de la fecha")
        return matriz
    tmp_data = pd.DataFrame()
    matriz_total = matriz
    tmp_data = tmp_data.append(
        matriz[matriz['6. Detalle'].str.contains(palabras_reservadas[0])],
        sort=True
    )
    '''
        pre -> in cod. -> next) 
    '''

    tmp_data['Prev Cod.'] = ["NA"] + tmp_data.iloc[:-1]['3. Codigo'].values.tolist()
    tmp_data['Next Cod.'] = tmp_data.iloc[1:]['3. Codigo'].values.tolist() + ["NA"]
    tmp_data['indexfin_next'] = tmp_data.iloc[1:]['index_fin'].values.tolist() + ["NA"]
    tmp_data = tmp_data.reset_index(drop=True)

    # print(tmp_data[['2. Hora', 'Prev Cod.', 'Next Cod.', '3. Codigo']].to_string())
    # exit()
    # Eval Inicios
    inicios_pat = pd.DataFrame()
    inicios_pat = inicios_pat.append(tmp_data[tmp_data.apply(findPatronIni, axis=1)], sort=True)
    # print(inicios_pat[['2. Hora','Prev Cod.','Next Cod.','3. Codigo']].to_string())

    # Eval Finales
    finales_pat = pd.DataFrame()
    finales_pat = finales_pat.append(tmp_data[tmp_data.apply(findPatronFin, axis=1)], sort=True)

    # print(finales_pat[['2. Hora','Prev Cod.','Next Cod.','3. Codigo']].to_string())
    # exit()
    # exit()
    # Get Time Transaccion Ini, Fin
    hora_inicio = []
    hora_inicio_fin = []
    # Inicios

    for i in range(len(inicios_pat.axes[0])):
        nfila = inicios_pat.iloc[i:i + 1]

        idd = nfila['indexfin_next'].iloc[0]

        # hora_inicio.append(nfila['Up Protegiendo'].values.tolist()[0])

        try:
            hora_inicio_q = tmp_data[(tmp_data['indexfin_next'] == idd) &
                                     (tmp_data['3. Codigo'] == nfila['Prev Cod.'].iloc[0]) &
                                     (tmp_data['Next Cod.'] == nfila['3. Codigo'].iloc[0])]['2. Hora'].iloc[0]
        except IndexError:
            hora_inicio_q = tmp_data[tmp_data['indexfin_next'] == nfila['index_fin'].iloc[0]]['2. Hora'].iloc[0]

        hora_inicio.append(hora_inicio_q)
        hora_inicio_fin.append(nfila['Down Desprotegiendo'].values.tolist()[0])

    hora_fin = []
    new_indexfin = []

    #print(tmp_data.to_string())
    #exit()
    # Finales
    for i in range(len(finales_pat.axes[0])):
        nfila = finales_pat.iloc[i:i + 1]
        #print(nfila.to_string())
        index = nfila['index'].values.tolist()[0]
        operativa = nfila['3. Codigo'].values.tolist()[0]

        if not operativa in list_cod_especiales:
            # next_code = nfila['Next Cod.'].values.tolist()[0] tiempo = matriz_total[(matriz_total['3. Codigo'] ==
            # next_code) & (matriz_total['index'] > index)].iloc[0:1][ 'Up Desprotegiendo'].values.tolist()[0]
            idd = nfila['indexfin_next'].iloc[0]

            try:
                hora_fin_q = tmp_data[(tmp_data['index_fin'] == idd) &
                                      (tmp_data['3. Codigo'] == nfila['Next Cod.'].iloc[0]) &
                                      (tmp_data['Prev Cod.'] == nfila['3. Codigo'].iloc[0])]['2. Hora'].iloc[0]
            except IndexError:
                hora_fin_q = tmp_data[tmp_data['indexfin_next'] == nfila['index_fin'].iloc[0]]['2. Hora'].iloc[0]
            # print(hora_fin_q)
            # exit("tiempo")
            hora_fin.append(hora_fin_q)

            index_next_fin = nfila['indexfin_next'].values.tolist()[0]
            if index_next_fin != 'NA':
                new_indexfin.append(index_next_fin + 1)
            else:
                new_indexfin.append(index)
        else:
            hora_fin.append(nfila['Down Desprotegiendo'].values.tolist()[0])
            index_despro = \
                matriz_total[(matriz_total['3. Codigo'] == 'Desprotegiendo') & (matriz_total['index'] > index)].iloc[
                0:1]['index'].values.tolist()[0]
            if index_despro != 'NA':
                new_indexfin.append(index_despro + 1)
            else:
                new_indexfin.append(index)

    inicios_pat['Hora Ini'] = hora_inicio
    inicios_pat['Hora Fin'] = hora_inicio_fin
    inicios_pat['Iter.'] = "->"

    finales_pat['Hora Ini'] = "-"
    finales_pat['Hora Fin'] = hora_fin
    finales_pat['Iter.'] = "<-"

    # Reemplzar index fin x index inicial (para incluir tiempos de usuario y suboperacion)
    finales_pat['index'] = new_indexfin

    tmp_x = inicios_pat.append(finales_pat, sort=True).sort_values(by=['2. Hora', 'index'])

    if len(tmp_x.axes[0]) == 0:
        return tmp_x

    # Filtro para par de patrones incompletos/datos incompletos
    i = 0
    size = len(tmp_x.axes[0])
    tmp_data = pd.DataFrame()
    while i < size:
        nfila = tmp_x.iloc[i:i + 1]
        if nfila['Iter.'].values.tolist()[0] == '->' and nfila['Hora Fin'].values.tolist()[
            0] != 'NA' and i + 1 < size and tmp_x.iloc[i + 1:i + 2]['Iter.'].values.tolist() != '->':
            tmp_data = tmp_data.append(tmp_x.iloc[i, :], sort=True)
            i += 1
            nfila = tmp_x.iloc[i:i + 1]
            while i < size and nfila['Iter.'].values.tolist()[0] != '<-':
                i += 1
                nfila = tmp_x.iloc[i:i + 1]
            if i >= size:
                tmp_data = tmp_data.iloc[:-1, :]
            else:
                tmp_data = tmp_data.append(tmp_x.iloc[i, :], sort=True)
                i += 1

        else:
            i += 1

    '''
        Get sub-operaciones
    '''

    sub_oper = pd.DataFrame()

    for j in range(0, int(len(tmp_data.axes[0]) / 2)):
        try:
            index_i = int(tmp_data.iloc[(j * 2), 13])
            index_f = int(tmp_data.iloc[(j * 2) + 1, 12])
        except ValueError:
            index_i = int(tmp_data.iloc[(j * 2), 12])
            index_f = int(tmp_data.iloc[(j * 2) + 1, 12])

        rango = matriz_total[(matriz_total['index'] < index_f) & (matriz_total['index'] > index_i)]
        rango = rango.copy()
        try:
            rango.loc[:, 'Hora Ini'] = rango.loc[:, 'Up Protegiendo']
            rango.loc[:, 'Hora Fin'] = rango.loc[:, 'Down Desprotegiendo']
            rango.loc[:, 'Iter.'] = j + 1
        except ValueError:
            pass

        sub_oper = sub_oper.append(rango, sort=True)

    if len(tmp_data.axes[0]) == 0:
        return tmp_data

    tmp_data = tmp_data.append(sub_oper, sort=True).sort_values(by=['index'])
    tmp_data['Puesto'] = puesto

    tmp_data = tmp_data.rename(columns={'Prev Cod.': 'Ventana Predecesora',
                                        'Hora Ini': 'Hora Inicio',
                                        '1. Fecha': 'Fecha',
                                        '2. Hora': 'Hora de palabra reservada',
                                        '3. Codigo': 'Tx/Ventana'})
    return tmp_data[['Fecha', 'Puesto', 'Iter.',
                     'Hora Inicio', 'Hora Fin', 'Hora de palabra reservada',
                     'Ventana Predecesora',
                     'Tx/Ventana', 'Next Cod.',
                     'index']].reset_index(drop=True)


'''
    Formato reporte
'''


def formReportDatos(matriz):
    if matriz.size < 1:
        print("NOTIFICACIÓN: No hay datos")
        return matriz

    result = pd.DataFrame()
    tmp = pd.DataFrame()

    matriz['Prev'] = ["NA"] + matriz.iloc[:-1]['Tx/Ventana'].values.tolist()
    matriz['Next'] = matriz.iloc[1:]['Tx/Ventana'].values.tolist() + ["NA"]

    tmp = tmp.append(matriz[(matriz['Prev'] == "Protegiendo") | (matriz['Prev'] == "Desprotegiendo") |
                            (matriz['Next'] == "Protegiendo") | (matriz['Next'] == "Desprotegiendo")], sort=True)

    # Obtener extremos de control de usuario
    extremos = tmp[(tmp['Tx/Ventana'] != "Desprotegiendo")
                   & (tmp['Tx/Ventana'] != "Protegiendo")].reset_index(drop=True)
    result = result.append(extremos, sort=True)

    for i in range(len(extremos.axes[0]) - 1):
        # Intervalos ini - fin

        ini = extremos.iloc[i:i + 1]['index'].values.tolist()[0]
        fin = extremos.iloc[i + 1:i + 2]['index'].values.tolist()[0]

        # Obtener data entre intervalos
        rango = tmp[(tmp['index'] > ini) & (tmp['index'] < fin)]

        # Condicionales
        tam = len(rango.axes[0])
        if tam > 0:
            if tam < 3:
                # Añadir solo las 2 columnas
                result = result.append(rango, sort=True)
            elif tam % 2 != 0:
                # Acortar tiempo usuario y añadir palabra reservada restante
                dato_usuario = rango.iloc[1, :]
                dato_usuario = dato_usuario.copy()

                dato_usuario['Tx/Ventana'] = "Usuario"
                dato_usuario['Hora Inicio'] = rango.iloc[1]['Hora de palabra reservada']
                dato_usuario['Hora Fin'] = rango.iloc[-1]['Hora de palabra reservada']
                dato_usuario['Hora de palabra reservada'] = "-"

                result = result.append(rango.iloc[0, :], sort=True)
                result = result.append(dato_usuario, sort=True)

            else:
                # Añadir extremos y acortar
                dato_usuario = rango.iloc[1, :]
                dato_usuario = dato_usuario.copy()

                dato_usuario['Tx/Ventana'] = "Usuario"
                dato_usuario['Hora Inicio'] = rango.iloc[1]['Hora de palabra reservada']
                dato_usuario['Hora Fin'] = rango.iloc[-2]['Hora de palabra reservada']
                dato_usuario['Hora de palabra reservada'] = "-"

                result = result.append(rango.iloc[-1:, :], sort=True)
                result = result.append(rango.iloc[0, :], sort=True)
                result = result.append(dato_usuario, sort=True)

    # Añadir datos omitidos
    result = result.append(matriz[(matriz['Prev'] != "Protegiendo") & (matriz['Prev'] != "Desprotegiendo") &
                                  (matriz['Next'] != "Protegiendo") & (matriz['Next'] != "Desprotegiendo")], sort=True)

    # del result['Prev']
    # del result['Next']

    result = result[(result['Tx/Ventana'] != "Protegiendo") & (result['Tx/Ventana'] != "Desprotegiendo")]

    return result.sort_values(by=['index']).reset_index(drop=True)


def convertDate(datestr):
    if datestr.count(':') == 2:
        return datestr
    nt = ""
    cont = 1
    for i in datestr:
        if i == ':':
            if cont == 3:
                nt += "."
                continue
            cont = cont + 1
        nt += i
    return nt


# Aca se elimina el index general
def formReportDetalle(matriz, ind_ocurrencias, ind_ocurrencias_flujo, cod_oficina, li_ofi, num_puesto):
    global h_ini
    cod_oficina = cod_oficina[2:5]

    result = pd.DataFrame()
    result_no_oper = pd.DataFrame()

    inicios = matriz[matriz['Iter.'] == '->']
    finales = matriz[matriz['Iter.'] == '<-']

    dic_iter_operativa = iniIterfromOperativa()
    dic_iter_flujo = initIterfromFlujo()
    datos_resumen = []

    for i in range(0, len(inicios.axes[0])):
        '''
            General
        '''
        esce = inicios.iloc[i]['Ventana Predecesora']
        cod_oper = inicios.iloc[i]['Tx/Ventana']
        # Operativa
        operativa = getNamefromCodOperativa(cod_oper)
        # Ini
        ini = inicios.iloc[i]['index']
        h_ini = inicios['Hora Inicio'].iloc[i]
        h_ini_ope = inicios['Hora de palabra reservada'].iloc[i]

        # Fin
        fin = finales.iloc[i]['index']
        h_fin = finales.iloc[i]['Hora Fin']
        h_fin_ope = finales['Hora de palabra reservada'].iloc[i]

        # Hora de operativa
        tiempo_operativa = difTime(h_ini_ope, h_fin_ope)

        # Hora total sistema
        tiempo_total = difTime(h_ini, h_fin)

        # Saber si se realizó la operacion
        is_operation, tipo_flujo, fin, time_escenario = isOperation(matriz, ini, fin, cod_oper, esce, h_ini)
        #        print(time_escenario, h_ini)
        if str(h_ini) == 'NA':
            h_ini = '00:00:01.000'

        hora_ini_esc = dt.strptime(h_ini, "%H:%M:%S.%f")
        hora_ini_ope = dt.strptime(h_ini_ope, "%H:%M:%S.%f")
        if hora_ini_ope > hora_ini_esc:
            time_escenario = str(hora_ini_ope - hora_ini_esc)
        else:
            time_escenario = str(hora_ini_esc - hora_ini_ope)

        # Ocurrencia x Operativa
        dic_iter_operativa[cod_oper][str(is_operation)] += 1
        iteracion = dic_iter_operativa[cod_oper][str(is_operation)]
        # Ocurrencia x Dia
        ind_ocurrencias[cod_oper][str(is_operation)] += 1
        iteracion_dia = ind_ocurrencias[cod_oper][str(is_operation)]

        # Sub Tx/Ventanas
        rango, subtime_disp_real_A, subtime_disp_real_B, ventanas, transacciones = formatSubOperaciones(matriz, ini,
                                                                                                        fin,
                                                                                                        iteracion=iteracion,
                                                                                                        iteracion_dia=iteracion_dia,
                                                                                                        operativa=operativa,
                                                                                                        escenario=esce,
                                                                                                        cod_operativa=cod_oper,
                                                                                                        tiempo_total=tiempo_total)
        # Total de tramos
        try:
            total_tramos = str(rango.iloc[-1]['Tramo'])
            rango['Total Tramos'] = total_tramos
        except IndexError:
            continue

        '''
            Determinar tipo de flujo
            Flujo correcto/ Flujo no identificado/ Detalle no operacion)
        '''

        if is_operation:
            tipo_flujo = getTipoFlujofromListCod(ventanas, cod_oper)
            tipo_flujo += getNameExeTx(transacciones, cod_oper)

        ##Indices de flujos
        if dic_iter_flujo.get(tipo_flujo):
            dic_iter_flujo[tipo_flujo] += 1
        else:
            dic_iter_flujo[tipo_flujo] = 1

        ##Indices de flujo global (dia)
        if ind_ocurrencias_flujo.get(tipo_flujo):
            ind_ocurrencias_flujo[tipo_flujo] += 1
        else:
            ind_ocurrencias_flujo[tipo_flujo] = 1

        rango['Flujo'] = tipo_flujo
        rango['Iter. Flujo'] = dic_iter_flujo[tipo_flujo]
        rango['Iter. Flujo Oficina'] = ind_ocurrencias_flujo[tipo_flujo]
        # rango['Hora_ini_escenario'] = h_ini
        # rango['Hora_ini_operativa'] = tiempo_operativa_ini
        # rango['Hora_escenario'] = time_escenario
        # Identificador de detalle
        identificador = str(is_operation)[0] + cod_oficina + \
                        str(ind_ocurrencias[cod_oper][str(is_operation)]) \
                        + esce + cod_oper + str(inicios.iloc[i, 6])

        rango['ID'] = identificador

        # Separar operativos / no operativos

        if is_operation:
            result = result.append(rango, sort=True)
        else:
            result_no_oper = result_no_oper.append(rango, sort=True)

        datos_adicionales_resumen = {
            'Identificador': identificador,
            'Codigo Oficina': cod_oficina,
            'Total Tramos': total_tramos,
            'Tiempo Disponible Real': sumTime(subtime_disp_real_A, subtime_disp_real_B),
            'Tiempo Disponible Real A': subtime_disp_real_A,
            'Tiempo Disponible Real B': subtime_disp_real_B,
            'Is Operation': is_operation,
            'Tipo Flujo': tipo_flujo,
            'Iter Flujo': dic_iter_flujo[tipo_flujo],
            'Iter Flujo Oficina': ind_ocurrencias_flujo[tipo_flujo],
            'Hora_escenario': time_escenario,
            'Terminal': li_ofi[cod_oficina][num_puesto]['terminal'],
            'Usuario': li_ofi[cod_oficina][num_puesto]['usuario'],
            'Hora_ini_escenario': h_ini,
            'Hora_fin_escenario': h_fin,
            'Hora_ini_operativa': h_ini_ope,
            'Hora_fin_operativa': h_fin_ope,
            'Hora_operativa': tiempo_operativa
        }
        datos_resumen.append(datos_adicionales_resumen)

    result['Codigo Oficina'] = cod_oficina
    result_no_oper['Codigo Oficina'] = cod_oficina

    # result['Terminal'] = li_ofi[cod_oficina][num_puesto]['terminal']
    # result['Usuario'] = li_ofi[cod_oficina][num_puesto]['usuario']

    # result_no_oper['Terminal'] = li_ofi[cod_oficina][num_puesto]['terminal']
    # result_no_oper['Usuario'] = li_ofi[cod_oficina][num_puesto]['usuario']
    if len(result.axes[0]) != 0:
        result = result[['ID', 'Codigo Oficina', 'Fecha', 'Iter. Oficina', 'Puesto',
                         'Iter.', 'Escenario', 'Operativa', 'Ventana inicial',
                         'Tiempo Operativa', 'Tiempos Parciales',
                         'Hora Inicio', 'Hora Fin', 'Hora de palabra reservada',
                         'Tx/Ventana', 'Detalle Tx/Ventana', 'Tramo', 'Total Tramos', 'Flujo', 'Iter. Flujo',
                         'Iter. Flujo Oficina']]  # , 'Hora_escenario','Terminal','Usuario','Hora_ini_escenario','Hora_ini_operativa']]

    if len(result_no_oper.axes[0]) != 0:
        result_no_oper = result_no_oper[['ID', 'Codigo Oficina', 'Fecha', 'Iter. Oficina', 'Puesto',
                                         'Iter.', 'Escenario', 'Operativa', 'Ventana inicial',
                                         'Tiempo Operativa', 'Tiempos Parciales',
                                         'Hora Inicio', 'Hora Fin', 'Hora de palabra reservada',
                                         'Tx/Ventana', 'Detalle Tx/Ventana', 'Tramo', 'Total Tramos', 'Flujo',
                                         'Iter. Flujo', 'Iter. Flujo Oficina', ]]
        # 'Hora_escenario','Terminal','Usuario', 'Hora_ini_escenario', 'Hora_ini_operativa']]

    return {
        'Resultados Correctos': result,
        'Resultados Incorrectos': result_no_oper,
        'Indice Ocurrencias': ind_ocurrencias,
        'Indice Ocurrencias Flujo': ind_ocurrencias_flujo,
        'Datos Totales Resumen': datos_resumen
    }


# Determina si se realizó la operacion es correcta/flujo de operacion
def isOperation(matriz, ini, fin, cod_operativa, escenario, i):
    rango = matriz[(matriz['index'] >= ini) & (matriz['index'] <= fin)]
    code_exception = getListCodExcepFC(cod_operativa)
    code_exception_flujos = getListCodFlujos(cod_operativa)

    ecx = rango[rango['Tx/Ventana'].isin(['LAPEVEPE28PE2801C'])]

    if len(ecx.axes[0]) > 0:
        ope = True
        if len(rango[rango['Tx/Ventana'].isin(['LABGVEB632B63201C'])].axes[0]) > 0:  # buscar depósito
            cod_operativa = "LABGVEB632B63201C"
            ope = False
        if len(rango[rango['Tx/Ventana'].isin(['LABGVEB503B50301C'])].axes[0]) > 0:  # buscar retiro
            cod_operativa = "LABGVEB503B50301C"
            ope = False
        if len(rango[rango['Tx/Ventana'].isin(['LAMCVEMPOPMPOP01C'])].axes[0]) > 0:  # buscar pago de tarjeta
            cod_operativa = "LAMCVEMPOPMPOP01C"
            ope = False

        if ope:
            return False, 'No se realizó Flujo con DNI', fin, '00:00:00.0'

    ecx = rango[rango['Tx/Ventana'].isin(['LABGVEB565B56501C'])]
    if len(ecx.axes[0]) > 0:
        ope = True
        if len(rango[rango['Tx/Ventana'].isin(['LABGVEB632B63201C'])].axes[0]) > 0:  # buscar depósito
            cod_operativa = "LABGVEB632B63201C"
            ope = False
        elif len(rango[rango['Tx/Ventana'].isin(['LABGVEB503B50301C'])].axes[0]) > 0:  # buscar retiro
            cod_operativa = "LABGVEB503B50301C"
            ope = False
        if ope:
            return False, 'No se realizó Flujo de Lectura de Tarjeta', fin, '00:00:00.0'

    if len(rango[rango['Tx/Ventana'].str.contains('Tx-' + getTxfromCodOperativa(cod_operativa))].axes[0]) == 0:
        return False, 'No se realizó Tx' + getTxfromCodOperativa(cod_operativa), fin, '00:00:00.0'
    else:

        error = False
        # Determinar operacion correcta
        mi_tx = rango[rango['Tx/Ventana'].str.contains('Tx-' + getTxfromCodOperativa(cod_operativa))]

        if len(mi_tx.axes[0]) >= 1:
            hin = rango['Hora de palabra reservada'][rango['Tx/Ventana'] == cod_operativa][
                ~(rango['Hora de palabra reservada'] == '-') &
                ~(rango['Hora de palabra reservada'] == 'NA')].values.tolist()
            if len(hin) > 1:
                time_escenario = hin[0]
            else:
                if len(hin) == 0:
                    time_escenario = '00:00:02.000'
                else:
                    time_escenario = hin[0]

            index = mi_tx.iloc[0]['index']
            ventanas_anteriores = rango[(rango['index'] < index) & ~(rango['Tx/Ventana'].str.contains('Tx-')) & (
                    rango['Tx/Ventana'] != 'Usuario')]
            tmp_ventanas = rango[(rango['index'] > index) & ~(rango['Tx/Ventana'].str.contains('Tx-')) & (
                    rango['Tx/Ventana'] != 'Usuario')]
            tmp_ventanas = tmp_ventanas.copy()
            tmp_ventanas['Next Cod.'] = tmp_ventanas['Tx/Ventana'].values.tolist()[1:] + ["NA"]

            esce_final = tmp_ventanas[
                (tmp_ventanas['Tx/Ventana'] == cod_operativa) & (tmp_ventanas['Next Cod.'] == escenario)]

            if len(esce_final.axes[0]) != 0:
                fin = int(esce_final.iloc[0]['index']) + 1

            # Validar
            tmp_ventanas = tmp_ventanas[tmp_ventanas['index'] < fin]

            lista_ventanas = list(set(tmp_ventanas['Tx/Ventana'].values.tolist()))
            lista_ventanas_ant = list(set(ventanas_anteriores['Tx/Ventana'].values.tolist()))

            # Evaluar codigos distintos
            cod_acept = [cod_operativa, escenario] + code_exception + code_exception_flujos + lista_ventanas_ant

            for v in lista_ventanas:
                if not v in cod_acept:
                    error = True
                    print("WARNING-INFO: (", getNamefromCodOperativa(cod_operativa), ") CODIGO NUEVO POR REGISTRAR ", v,
                          '00:00:00.0')

        if error:
            return False, 'No se cerró la ventana luego de la Tx' + getTxfromCodOperativa(
                cod_operativa), fin, '00:00:00.0'
        else:
            return True, 'Flujo por determinar', fin, time_escenario


def formatSubOperaciones(matriz, ini, fin, iteracion, iteracion_dia, operativa, escenario, cod_operativa, tiempo_total):
    rango = matriz[(matriz['index'] >= ini) & (matriz['index'] < fin) & ~(matriz['Tx/Ventana'].str.contains('Tx-'))]

    rango = rango.copy()
    rango_tx = matriz[(matriz['index'] >= ini) & (matriz['index'] < fin) & (matriz['Tx/Ventana'].str.contains('Tx-'))]
    rango_tx = rango_tx.copy()

    tmp_disponible = pd.DataFrame()
    add_reg = 0
    tiempos_parciales = []
    indice = 1
    tramos = []
    ventanas = []
    time_disp_real_A = dt.strptime('00:00:00.000', "%H:%M:%S.%f")
    time_disp_real_B = dt.strptime('00:00:00.000', "%H:%M:%S.%f")

    totalSecsA = 0
    totalSecsB = 0
    #print(rango[~(rango['Tx/Ventana'] == 'Usuario')].to_string())

    for i in range(0, len(rango.axes[0])):

        d = difTime(rango.iloc[i]['Hora Inicio'], rango.iloc[i]['Hora Fin'])

        # Añadir tramo al que pertenece

        if rango.iloc[i]['Tx/Ventana'] == 'Usuario':
            indice += 1
            tramos.append(indice)

            # Disponible
            datos = rango.iloc[i, :]
            datos = datos.copy()
            h_ini = rango.iloc[i]['Hora Inicio']
            h_fin = rango.iloc[i]['Hora Fin']
            datos['Hora Inicio'] = h_ini
            datos['Hora Fin'] = h_fin
            datos['Tx/Ventana'] = 'Disponible A'
            datos['Hora de palabra reservada'] = '-'
            datos['index'] = rango.iloc[i - 1]['index'] + 1
            tmp_disponible = tmp_disponible.append(datos)
            add_reg = 1
            ##Disp-tiempo-parciales
            t = difTime(h_ini, h_fin)
            tiempos_parciales.append(t)
            if '-1' in str(t):
                t = '00:00:00.000'

            ##Sum (Tiempo disponible real)
            #tmp = t.split('.')[0].split(':')
            timeParts = t.split('.')[0].split(':')
            totalSecsA += (int(timeParts[0]) * 60 + int(timeParts[1])) * 60 + int(timeParts[2]) + \
                          round(float('0.' + t.split('.')[1]))

            '''time_disp_real_A += td(hours=int(tmp[0]),
                                   minutes=int(tmp[1]),
                                   seconds=int(tmp[2]),
                                   milliseconds=int(t.split('.')[1]))'''

            ##Tramo
            indice += 1
            tramos.append(indice)
            indice += 1

            tiempos_parciales.append(d)

        else:
            # Disponible
            if add_reg:
                # Disponible
                datos = rango.iloc[i - 1, :]
                datos = datos.copy()
                h_ini = rango.iloc[i - 1]['Hora Inicio']
                h_fin = rango.iloc[i]['Hora Fin']

                datos['Hora Inicio'] = h_ini
                datos['Hora Fin'] = h_fin
                datos['Tx/Ventana'] = 'Disponible B'
                datos['Hora de palabra reservada'] = '-'
                datos['index'] = rango.iloc[i - 1]['index'] + 1
                tmp_disponible = tmp_disponible.append(datos)
                add_reg = 0

                ##Dis-tiempo-parciales
                t = difTime(h_ini, h_fin)
                tiempos_parciales.append(t)

                ##Sum (Tiempo disponible real)
                if '-1' in str(t):
                    t = '00:00:00.000'

                #tmp = t.split('.')[0].split(':')
                timeParts = t.split('.')[0].split(':')
                totalSecsB += (int(timeParts[0]) * 60 + int(timeParts[1])) * 60 + int(timeParts[2]) + \
                              round(float('0.' + t.split('.')[1]))
                '''time_disp_real_B += td(hours=int(tmp[0]),
                                       minutes=int(tmp[1]),
                                       seconds=int(tmp[2]),
                                       milliseconds=int(t.split('.')[1]))'''
                ##Tramos
                tramos.append(indice)
                indice += 1

            # Ventanas
            ventanas.append(rango.iloc[i]['Tx/Ventana'])

            # Tramo
            tiempos_parciales.append(d)
            tramos.append(indice)

    totalSecsA, sec = divmod(totalSecsA, 60)
    hr, min = divmod(totalSecsA, 60)
    time_disp_real_A = "%d:%02d:%02d" % (hr, min, sec)

    totalSecsB, sec = divmod(totalSecsB, 60)
    hr, min = divmod(totalSecsB, 60)
    time_disp_real_B = "%d:%02d:%02d" % (hr, min, sec)

    # format time
    if '.' in str(time_disp_real_A):
        time_disp_real_A = str(time_disp_real_A).split(' ')[-1][1:-3]
    else:
        time_disp_real_A = str(time_disp_real_A) + ".000"
    if '.' in str(time_disp_real_B):
        time_disp_real_B = str(time_disp_real_B).split(' ')[-1][1:-3]
    else:
        time_disp_real_B = str(time_disp_real_B) + ".000"

    rango = rango.append(tmp_disponible).sort_values(by=['index'])

    rango['Tiempos Parciales'] = tiempos_parciales
    rango['Tramo'] = tramos

    rango_tx['Tiempos Parciales'] = '-'
    rango_tx['Tramo'] = 'NA'
    rango = rango.append(rango_tx).sort_values(by=['index'])
    rango = rango[sorted(rango.columns)]

    # Añade tramo de Tx y Detalle Tx/Ventana
    nuevos_tramos = []
    detalle_cod_tx_ventana = []
    lista_detalle_codigos = iniDetailCod_Tx_Ventana()

    for j in range(0, len(rango.axes[0])):
        # Detalle de Tx/Ventana
        detalle_cod_tx_ventana.append(
            getDetailCod_Tx_Ventana(lista_detalle_codigos, str(rango.iloc[j]['Tx/Ventana'])).get('Detalle'))

        if 'Tx-' in rango.iloc[j]['Tx/Ventana']:
            if not rango.iloc[j - 1]['Tx/Ventana'] in ['Usuario', 'Disponible A', 'Disponible B'] and not 'Tx-' in \
                                                                                                          rango.iloc[
                                                                                                              j - 1][
                                                                                                              'Tx/Ventana']:
                nuevos_tramos.append(rango.iloc[j - 1]['Tramo'])
            else:
                a = 1
                try:
                    while rango.iloc[j + a]['Tx/Ventana'] in ['Usuario', 'Disponible A', 'Disponible B'] or 'Tx-' in \
                            rango.iloc[
                                j + a]['Tx/Ventana']:
                        a += 1
                    nuevos_tramos.append(rango.iloc[j + a]['Tramo'])
                except IndexError:
                    nuevos_tramos.append(rango.iloc[j - 1]['Tramo'])

        else:
            nuevos_tramos.append(rango.iloc[j]['Tramo'])

    rango['Detalle Tx/Ventana'] = detalle_cod_tx_ventana
    rango['Tramo'] = nuevos_tramos
    rango['Iter. Oficina'] = iteracion_dia
    rango['Iter.'] = iteracion
    rango['Operativa'] = operativa
    rango['Escenario'] = escenario
    rango['Ventana inicial'] = cod_operativa
    rango['Tiempo Operativa'] = tiempo_total

    rango['Hora Inicio'] = rango['Hora Inicio'].replace('NA', '00:00:00.000')
    # del rango['Ventana Predecesora']
    del rango['Next Cod.']
    del rango['index']

    return rango, time_disp_real_A, time_disp_real_B, ventanas, rango_tx['Tx/Ventana'].values.tolist()


# noinspection PyTypeChecker
def formReportResumen(matriz, tmp_ocurrencias, datos_totales_resumen):
    result = []
    result_no_oper = []
    resumen_df = []

    inicios = matriz[matriz['Iter.'] == '->']
    finales = matriz[matriz['Iter.'] == '<-']

    dic_oper = iniIterfromOperativa()

    for i in range(0, len(inicios.axes[0])):

        # Resumen de datos obtenidos de detalle
        try:
            datos_resumen = datos_totales_resumen[i]
        except IndexError:
            continue

        # Es operativa
        is_operation = datos_resumen.get('Is Operation')

        datos = inicios.iloc[i][['Fecha', 'Puesto']].values.tolist()
        h_ini = '00:00:00.000' if inicios.iloc[i]['Hora Inicio'] == 'NA' else inicios.iloc[i]['Hora Inicio']
        h_fin = '00:00:00.000' if finales.iloc[i]['Hora Fin'] == 'NA' else finales.iloc[i]['Hora Fin']
        esce = inicios.iloc[i]['Ventana Predecesora']
        oper = inicios.iloc[i]['Tx/Ventana']
        ini = inicios.iloc[i]['index']
        fin = finales.iloc[i]['index']

        tiempo_total = difTime(h_ini, h_fin)

        if "1900" in str(tiempo_total):
            tiempo_total = "00:00:00.000"
        if "-1" in str(tiempo_total):
            tiempo_total = "00:00:00.000"

        tmp_tiempo = getTimeSubOper(matriz, ini, fin)
        if "1900" in str(tmp_tiempo):
            tmp_tiempo = "00:00:00.000"
        if "-1" in str(tmp_tiempo):
            tmp_tiempo = "00:00:00.000"
        tiempo_nacar = tmp_tiempo[0]
        tiempo_user = tmp_tiempo[1]

        # Iter cod_oper
        dic_oper[oper][str(is_operation)] += 1
        nom_oper = getNamefromCodOperativa(oper)

        # Iter_x_dia_x_oper
        tmp_ocurrencias[oper][str(is_operation)] += 1

        tiempo_disp = difTime(tiempo_nacar, tiempo_total)

        # Tiempo disponible neto
        disp_neto = difTime(tiempo_nacar, tiempo_total)

        # noinspection PyTypeChecker
        data = [datos_resumen.get('Identificador')] + datos + [datos_resumen.get('Codigo Oficina')] + [
            tmp_ocurrencias[oper][str(is_operation)]] \
               + [nom_oper] + [dic_oper[oper][str(is_operation)]] + [h_ini, h_fin, esce, oper] \
               + [tiempo_total, tiempo_nacar,
                  tiempo_disp, tiempo_user, disp_neto,
                  datos_resumen.get('Tiempo Disponible Real'),
                  datos_resumen.get('Tiempo Disponible Real A'),
                  datos_resumen.get('Tiempo Disponible Real B'),
                  datos_resumen.get('Total Tramos'),
                  datos_resumen.get('Iter Flujo Oficina'),
                  datos_resumen.get('Iter Flujo'),
                  datos_resumen.get('Tipo Flujo'),
                  datos_resumen.get('Hora_escenario'),
                  datos_resumen.get('Terminal'),
                  datos_resumen.get('Usuario'),
                  datos_resumen.get('Hora_ini_escenario'),
                  datos_resumen.get('Hora_ini_operativa'),
                  datos_resumen.get('Hora_fin_escenario'),
                  datos_resumen.get('Hora_fin_operativa'),
                  datos_resumen.get('Hora_operativa')
                  ]

        if is_operation:
            list_key = ['identificador', 'fecha', 'puesto',
                        'codigo_oficina', 'iter_oficina', 'nombre_operativa', 'iter_puesto', 'hora_inicio', 'hora_fin',
                        'codigo_escenario', 'codigo_operativa', 'tiempo_operativa',
                        'tiempo_sistema', 'tiempo_disponible', 'tiempo_usuario', 'tiempo_disponible_neto',
                        'tiempo_disponible_rea', 'tiempo_disponible_real_a', 'tiempo_disponible_real_b',
                        'numero_tramos', 'iter_flujo_puesto', 'iter_flujo_oficina', 'nombre_flujo',
                        'Hora_escenario', 'Terminal', 'Usuario', 'Hora_ini_escenario', 'Hora_ini_operativa',
                        'Hora_fin_operativa', 'Hora_fin_escenario', 'Hora_operativa',
                        ]

            resumen_df.append(dict(zip(list_key, data)))
            result.append(data)
        else:
            result_no_oper.append(data)

    result_text = ""
    result_text_no_oper = ""

    for a in result:
        p = [str(x) for x in a]
        result_text = result_text + str(','.join(p)) + "\n"

    for b in result_no_oper:
        p = [str(x) for x in b]
        result_text_no_oper = result_text_no_oper + str(','.join(p)) + "\n"

    print("INFO: Total registro de Operativas obtenidas (b)", len(result))

    print("INFO: Total registro de Operativas obtenidas (m)", len(result_no_oper))

    return {
        'result': result,
        'result_df': resumen_df,
        'result_string': result_text[:-1],
        'result_no_oper': result_no_oper,
        'result_no_oper_string': result_text_no_oper[:-1],
    }


def getTimeSubOper(data, a, b):
    nacar = dt.strptime('00:00:00.000', "%H:%M:%S.%f")
    usuario = dt.strptime('00:00:00.000', "%H:%M:%S.%f")

    data_ope = data[(data['Tx/Ventana'] != 'Usuario') & (data['index'] > a) & (data['index'] < b)]
    data_user = data[(data['Tx/Ventana'] == 'Usuario') & (data['index'] > a) & (data['index'] < b)]

    # Get sistema time
    dic_times_nacar = {}

    for i in range(len(data_ope.axes[0])):

        h_ini = data_ope.iloc[i]['Hora Inicio']
        h_fin = data_ope.iloc[i]['Hora Fin']

        a = difTime(h_ini, h_fin)

        if len(a.split(':')) >= 3:
            dic_times_nacar[str(h_ini) + str(h_fin)] = a
    # Lista de tiempos

    times_nacar = []
    if len(dic_times_nacar) > 0:
        times_nacar = dic_times_nacar.values()

    if len(times_nacar) > 1:
        totalSecs = 0
        for t in times_nacar:
            if '-1' in str(t):
                t = '00:00:00.000'
            timeParts = t.split('.')[0].split(':')
            totalSecs += (int(timeParts[0]) * 60 + int(timeParts[1])) * 60 + int(timeParts[2]) + \
                         round(float('0.' + t.split('.')[1]))

        totalSecs, sec = divmod(totalSecs, 60)
        hr, min = divmod(totalSecs, 60)
        nacar = "%d:%02d:%02d" % (hr, min, sec)
        if '.' in str(nacar):
            nacar = str(nacar).split(' ')[-1][1:-3]
        else:
            nacar = str(nacar) + ".000"

    elif len(times_nacar) == 1:
        for a in times_nacar:
            nacar = a

    # Get user time
    times_user = []
    for j in range(len(data_user.axes[0])):
        h_ini = data_user.iloc[j]['Hora Inicio']
        h_fin = data_user.iloc[j]['Hora Fin']

        a = difTime(h_ini, h_fin)
        if len(a.split(':')) >= 3:
            times_user.append(a)

    if len(times_user) > 1:
        totalSecs = 0
        for t in times_user:
            # tmp = t.split('.')[0].split(':')

            timeParts = t.split('.')[0].split(':')
            totalSecs += (int(timeParts[0]) * 60 + int(timeParts[1])) * 60 + int(timeParts[2]) + \
                         round(float('0.' + t.split('.')[1]))

            '''usuario += td(hours=int(tmp[0]),
                          minutes=int(tmp[1]),
                          seconds=int(tmp[2]),
                          milliseconds=int(t.split('.')[1]))'''
        totalSecs, sec = divmod(totalSecs, 60)
        hr, min = divmod(totalSecs, 60)
        usuario = "%d:%02d:%02d" % (hr, min, sec)
        if '.' in str(usuario):
            usuario = str(usuario).split(' ')[-1][1:-3]
        else:
            usuario = str(usuario) + ".000"

    elif len(times_user) == 1:
        usuario = times_user[0]
    else:
        usuario = '0:00:00.000'

    return [nacar, usuario]


def difTime(ini, fin):
    if str(ini) == '-':
        ini = '0:00:00.000'
    if str(fin) == '-':
        fin = '0:00:00.000'
    if ini == fin:
        return '0:00:00.000'
    # Format time
    if str(fin).find('.') > -1:
        stime = "%H:%M:%S.%f"
    else:
        stime = "%H:%M:%S:%f"

    try:
        ini = dt.strptime(ini, stime)
        fin = dt.strptime(fin, stime)
        if ini > fin:
            rpt = str(ini - fin)
        else:
            rpt = str(fin - ini)

        # rpt = str(dt.strptime(fin, stime) - dt.strptime(ini, stime))
    except (ValueError, TypeError):
        return "00:00:00.000"

    if '.' in rpt:
        return rpt
    else:
        return rpt + '.000'


def sumTime(a, b):
    time_disp_real = dt.strptime('00:00:00.000', "%H:%M:%S.%f")
    totalSecs = 0
    for t in [a, b]:
        timeParts = t.split('.')[0].split(':')
        totalSecs += (int(timeParts[0]) * 60 + int(timeParts[1])) * 60 + int(timeParts[2]) + \
                     round(float('0.' + t.split('.')[1]))

    totalSecs, sec = divmod(totalSecs, 60)

    hr, min = divmod(totalSecs, 60)
    time_disp_real = "%d:%02d:%02d" % (hr, min, sec)

    '''tmp = a.split('.')[0].split(':')
    time_disp_real += td(hours=int(tmp[0]),
                         minutes=int(tmp[1]),
                         seconds=int(tmp[2]),
                         milliseconds=int(a.split('.')[1]))
    tmp = b.split('.')[0].split(':')
    time_disp_real += td(hours=int(tmp[0]),
                         minutes=int(tmp[1]),
                         seconds=int(tmp[2]),
                         milliseconds=int(b.split('.')[1]))'''

    if '.' in str(time_disp_real):
        time_disp_real = str(time_disp_real).split(' ')[-1][1:-3]
    else:
        time_disp_real = str(time_disp_real) + ".000"

    return time_disp_real


'''
    Relacion entre codigos y operaciones
'''


def iniIterfromOperativa():
    dic_operativas = {
        'LABGVEB632B63201C': {'True': 0, 'False': 0},
        'LAPEVEPE28PE2801C': {'True': 0, 'False': 0},
        'LABGVEB503B50301C': {'True': 0, 'False': 0},
        'LABGVEB565B56501C': {'True': 0, 'False': 0},
        'LARCVERC70RC7001C': {'True': 0, 'False': 0},
        'LAMCVEMPOPMPOP01C': {'True': 0, 'False': 0},
    }
    return dic_operativas


def initIterfromFlujo():
    lista_flujos = {
        'Depósito Cheques': 0,
        'Depósito Cheques con Lineas a Pantalla': 0,
        'Depósito Cheques con Tx Significativas': 0,
        'Depósito Cheques con Tx Significativas con Lineas a Pantalla': 0,
        'Depósito con Lineas a Pantalla': 0,
        'Depósito Cuenta Recaudadora': 0,
        'Depósito Cuenta Recaudadora con Tx Significativas': 0,
        'Depósito Especial': 0,
        'Depósito Especial con Tx Significativas': 0,
        'Depósito con N° de Cuenta': 0,
        'Depósito Normal + Ayuda': 0,
        'Depósito con DNI': 0,
        'Depósito con N° de Cuenta + Tx Significativas': 0,
        'Depósito Tx Significativas con mod. de datos': 0,
        'Depósito Tx Significativas con mod. de datos con Lineas a Pantalla': 0,

        # Siempre añadir, para flujos no determinados
        'FLUJO NO DETERMINADO': 0,
        'FLUJO TOTAL NO DETERMINADO': 0
    }
    return lista_flujos


def getTxfromCodOperativa(codigo):
    dic_operativas = {
        'LABGVEB632B63201C': 'B633',
        'LABGVEB503B50301C': 'B503',
        'LARCVERC70RC7001C': 'RC70',
        'LAMCVEMPOPMPOP01C': 'MP20',
    }

    if dic_operativas.get(codigo):
        return dic_operativas[codigo]
    else:
        print(str(codigo) + " - NO REGISTRADA")
        return "NO REGISTRADA"


def getNamefromCodOperativa(codigo):
    dic_operativas = {
        'LABGVEB632B63201C': 'DEPOSITO EN EFECTIVO',
        'LABGVEB503B50301C': 'RETIRO DE EFECTIVO',
        'LAPEVEPE28PE2801C': 'OPERACION CON DNI',
        'LABGVEB565B56501C': 'OPERACION CON LECTURA DE TARJETA',
        'LARCVERC70RC7001C': 'RECAUDOS Y PAGO DE SERVICIOS',
        'LAMCVEMPOPMPOP01C': 'PAGO DE TARJETA DE CREDITO'
    }

    if dic_operativas.get(codigo):
        return dic_operativas[codigo]
    else:
        return "NO REGISTRADA"


def getNameExeTx(lista_transacciones, cod_oper):
    flu = ""
    if "Tx-QG00" in lista_transacciones:
        flu += " Autorización"

    if "Tx-B851" in lista_transacciones:
        if lista_transacciones.count("Tx-B851") > 1:
            flu += " Impresion"
            return flu

    tx_oper = 'Tx-' + getTxfromCodOperativa(cod_oper)

    x_veces = lista_transacciones.count(tx_oper)

    return flu
    '''if x_veces > 1:
        return " - " + str(x_veces) + " ejecuciones"
    else:
        return ""'''


def getListCodExcepFC(cod_operation):
    dic_exceptions_cod = {
        'LABGVEB632B63201C': ['LAQGVEQG00QG0001C', 'LABGVEB503B63302C', 'LAISVEI007I00701C'],
        'LABGVEB503B50301C': ['LABGVEB503B63302C'],
    }

    if dic_exceptions_cod.get(cod_operation):
        return dic_exceptions_cod[cod_operation]
    else:
        return [cod_operation]


def getTipoFlujofromListCod(lista_codigos, cod_oper):
    result = {}
    resumen_codigos = list(set(lista_codigos))
    lista_flujos = getListFlujos(cod_oper)

    for k, v in lista_flujos.items():
        # Cumple todos los codigos del flujo
        rpt = True
        for cod in v[1:]:
            if not cod in resumen_codigos:
                rpt = False
                break
        if rpt:
            if result.get(v[1]):
                if v[0] > result.get(v[1])[0]:
                    result[v[1]] = [v[0], k, v[1:]]
            else:
                result[v[1]] = [v[0], k, v[1:]]

    if len(result) > 0:
        total_ventanas_map = getListCodFlujos(cod_oper) + getListCodExcepFlujo()

        if len(result) == 1:
            x = True
            for c in lista_codigos:
                if not c in total_ventanas_map:
                    return "FLUJO NO DETERMINADO"

            resp = ""
            for v in result.values():
                resp += v[1]

            return resp

        else:
            val = [a[0] for a in result.values()]
            tmayor = max(val)

            resp = ""
            keys = []
            for mayor in range(tmayor, 0, -1):
                for k, r in result.items():
                    cod_in_keys = True
                    subkeys = r[2]

                    if r[0] == mayor:
                        # Validar subcodigos en operaciones ya añadidas
                        if len(keys) > 0:
                            tmp_cod_inkeys = [True for f in range(len(keys))]
                            i = 0
                            for key_add in keys:
                                for s in subkeys:
                                    if not s in key_add:
                                        tmp_cod_inkeys[i] = False
                                        break
                                i += 1
                            # Si existe en una de las keys añadidas-> no agregar de nuevo
                            if not True in tmp_cod_inkeys:
                                cod_in_keys = False
                        else:
                            cod_in_keys = False

                        # Añadir codigos
                        if not cod_in_keys:
                            keys.append(subkeys)
                            resp += r[1] + '_'

            return resp[:-1]
    else:
        return "FLUJO TOTAL NO DETERMINADO"


def getListCodExcepFlujo():
    return ['LABGVEB503B63302C', 'LAPEVEPE1MPE1M01C']


def getDetailCod_Tx_Ventana(lista_detalle_codigos, codigo):
    if 'Tx-' in codigo:
        codigo = codigo.split('-')[1]
    else:
        if codigo in ['Usuario', 'Disponible A', 'Disponible B']:
            return {'Detalle': '-'}

    if lista_detalle_codigos.get(codigo):
        return lista_detalle_codigos[codigo]
    else:
        return {
            'Detalle': 'Nueva Tx/Ventana sin detalle'
        }

# Fin
