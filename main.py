# coding=utf-8
import logging
import os
import sys
from multiprocessing import Pool
import time
from queue import Queue
from threading import Thread

import conexion as dbconn
import nacarlogs as lg
import utilitarios as util
import pandas as pd
from nacarlogs import iniDetailCod_Tx_Ventana, getDetailCod_Tx_Ventana

# Variables globales
palabras_reservadas = [
    "Se va al conservar el contexto de la ventana destino",
    "Ejecutando transacci"
]

excep = [
    'Protegiendo', 'Desprotegiendo'
]


def getNombreVentanaTx(ven):
    if ven in excep:
        return "-"
    li = iniDetailCod_Tx_Ventana()
    ventana = getDetailCod_Tx_Ventana(li, ven)
    return ventana['Detalle']


def deleteProDespro(df):
    memo = {
        'pro': '',
        'desp': ''
    }

    for index, row in df.iterrows():

        if row['3. Codigo'] == excep[0]:
            memo['pro'] = row['index']
            continue

        if row['3. Codigo'] == excep[1]:
            if memo['pro'] == '':
                memo['desp'] = ''
                continue
            memo['desp'] = row['index']

            df = df[~df['index'].isin([memo['pro'], memo['desp']])]
            memo['pro'] = ''
            memo['desp'] = ''
            continue
        else:
            memo['pro'] = ''
            memo['desp'] = ''

    return df


def main(puesto, conn, fecha_eval, lista_cod_especiales, ind_ocurrencias, ind_ocurrencias_flujo,
         cad, excel):
    num_puesto = util.getPuestofromNom(puesto[0])
    cod_oficina = util.getCodOficina(puesto[0])
    tmp_ocurrencias = {}

    for k, v in ind_ocurrencias.items():
        tmp = {}
        for c, d in v.items():
            tmp[c] = d
        tmp_ocurrencias[k] = tmp

    # Obteniendo datos del dia, data preparada para su posterior análisis
    matriz_codigos_detalle, li_ofi = lg.getDatosxFecha(fecha_eval, palabras_reservadas, puesto)

    if excel:
        #pass
        gp = matriz_codigos_detalle.groupby('1. Fecha')
        dataLimpia = matriz_codigos_detalle.copy()
        dataLimpia['4. ventana/Tx'] = dataLimpia['3. Codigo'].apply(getNombreVentanaTx)
        del dataLimpia['6. Detalle']
        dataLimpia = dataLimpia.reindex(sorted(dataLimpia.columns), axis=1)
        #dataLimpia = deleteProDespro(dataLimpia)

        with pd.ExcelWriter('dataLimpia.xlsx') as writer:
            for nd in gp.groups.keys():
                dataLimpia[dataLimpia['1. Fecha'] == nd].to_excel(writer,sheet_name=nd.replace('/', '-'))
    # exit()

    # exit()
    if len(matriz_codigos_detalle.axes[0]) <= 0:
        print("INFO: No se han encontrado datos en la fecha", fecha_eval, "del puesto", num_puesto)
        return ind_ocurrencias, ind_ocurrencias_flujo

    # Filtrando por patron escenario->operacion
    matriz_procesada = lg.getDatosxPatron(matriz_codigos_detalle, num_puesto, lista_cod_especiales,
                                          palabras_reservadas)

    if len(matriz_procesada.axes[0]) <= 0:
        print("INFO: No se encontraron patrones que coincidan con la busqueda")
        return ind_ocurrencias, ind_ocurrencias_flujo

    # Reordenando, agrupando, etc, continua en el orden de los datos
    result_datos = lg.formReportDatos(matriz_procesada)

    response = lg.formReportDetalle(matriz=result_datos, ind_ocurrencias=ind_ocurrencias,
                                    ind_ocurrencias_flujo=ind_ocurrencias_flujo,
                                    cod_oficina=cod_oficina, li_ofi= li_ofi,
                                    num_puesto=num_puesto)

    if excel:
        with pd.ExcelWriter('dataFlujo.xlsx') as writer:
            for nd in response.keys():
                if str(type(response[nd])) == "<class 'pandas.core.frame.DataFrame'>":
                    response[nd].to_excel(writer, sheet_name=str(nd))
                else:
                    pd.DataFrame(list(response[nd])).to_excel(writer, sheet_name=str(nd))
        exit("fin")

    result_det = response.get('Resultados Correctos')
    result_det_no_oper = response.get('Resultados Incorrectos')
    ind_ocurrencias = response.get('Indice Ocurrencias')
    ind_ocurrencias_flujo = response.get('Indice Ocurrencias Flujo')
    totales_resumen = response.get('Datos Totales Resumen')

    # Generar data
    response_resumen = lg.formReportResumen(result_datos, tmp_ocurrencias, totales_resumen)


    '''
        Correctos
        Carga de archivo a BD / Archivo de respaldo
    '''
    # Data
    # result_detalle = util.formatCSV(result_det.values.tolist())
    # result_all = response_resumen.get('result_string')

    # Carga

    dbconn.insertResumen_Operativas(conn, response_resumen.get('result'), 'True', cad)
    dbconn.insertDetalle_Operativas(conn, result_det.replace({'-': None}).values.tolist(), 'True', cad)
    # util.genFile(output + 'reporte_detalle', result_detalle, 'txt')
    # util.genFile(output + 'reporte_resumen', result_all, 'txt')

    '''
        Incorrectos
        Carga de archivo a BD / Archivo de respaldo 
    '''
    if len(result_det_no_oper.axes[0]) != 0:
        # Data
        # result_detalle_no_oper = util.formatCSV(result_det_no_oper.values.tolist())
        # result_all_no_oper = response_resumen.get('result_no_oper_string')
        # Carga
        dbconn.insertResumen_Operativas(conn, response_resumen.get('result_no_oper'), 'False', cad)
        dbconn.insertDetalle_Operativas(conn, result_det_no_oper.replace({'-': None}).values.tolist(), 'False', cad)
        # util.genFile(output + 'reporte_detalle_notx', result_detalle_no_oper, 'txt')
        # util.genFile(output + 'reporte_resumen_notx', result_all_no_oper, 'txt')
        # pass

    # Retornar ocurrencias
    tam = len(result_det.axes[0])
    if tam > 0:
        print("INFO: Total registros (b)", tam)
        print("INFO: Total registros (m)", len(result_det_no_oper.axes[0]))

    return ind_ocurrencias, ind_ocurrencias_flujo


def operation_time(q, fecha_eval, output, conn, lista_especiales, ind_ocurrencias, ind_ocurrencias_flujo):
    while not q.empty():
        work = q.get()  # fetch new work from the Queue

        print("\n " + (60 * "*") + "\n ")
        try:
            util.getPuestofromNom(work[1][0])
        except IndexError:
            continue

        main(fecha_eval=fecha_eval, puesto=work[1], cad=cad, conn=conn, lista_cod_especiales=lista_especiales,
             ind_ocurrencias=ind_ocurrencias, ind_ocurrencias_flujo=ind_ocurrencias_flujo)

        logging.info("Requested..." + str(work[1]))

        # signal to the queue that task has been processed
        q.task_done()
    return True


def getLogsfromFolder(datos_conexion, carpeta, production, fecha_eval, output, lista_especiales, excel):
    # Obtener lista de archivos a evaluar (#)
    if carpeta[-1] != '/':
        carpeta += '/'

    total_nomfiles = util.getNomFiles(carpeta)
    ind_ocurrencias = lg.iniIterfromOperativa()
    ind_ocurrencias_flujo = lg.initIterfromFlujo()
    # Conexion BD
    if production:
        cad = '_prod'
    else:
        cad = '_test'

    conn = dbconn.initConn(datos_conexion, cad)

    '''q = Queue(maxsize=0)
    num_theads = min(60, len(total_nomfiles))
    # Populating Queue with tasks
    # load up the queue with the urls to fetch and the index for each job (as a tuple):
    for i in range(len(total_nomfiles)):
        # need the index and the url in each queue item.
        q.put((i, total_nomfiles[i]))

    start=time.time()
    for i in range(num_theads):
        logging.debug('Starting thread ', i)
        worker = Thread(target=operation_time,
                        args=(q, fecha_eval, output, conn, lista_especiales, ind_ocurrencias, ind_ocurrencias_flujo))
        worker.setDaemon(True)  # setting threads as "daemon" allows main program to
        # exit eventually even if these dont finish
        # correctly.
        worker.start()
    # now we wait until the queue has been processed
    q.join()
    '''
    start = time.time()
    # Recorrer todos los archivos por puesto
    for puesto in total_nomfiles:
        print("\n " + (60 * "*") + "\n ")
        try:
            util.getPuestofromNom(puesto[0])
        except IndexError:
            continue
        main(fecha_eval=fecha_eval, puesto=puesto, cad=cad, conn=conn, lista_cod_especiales=lista_especiales,
             ind_ocurrencias=ind_ocurrencias, ind_ocurrencias_flujo=ind_ocurrencias_flujo, excel=excel)

    print("end process: ", time.time() - start)

    # logging.info('All tasks completed.')
    conn.close()


def porOficina(carpeta, fecha_eval, config, production, excel):
    start = time.time()
    for f in os.listdir(carpeta):
        file = carpeta + f + '/'
        # fecha_eval = '11/02/2019'
        if os.path.isdir(file):
            # Recorrer todos los logs de oficina
            print("RESUMEN: FOLDER EVALUADO: ", file)
            getLogsfromFolder(carpeta=file,
                              fecha_eval=fecha_eval,
                              output=config.get('output'),
                              production=production,
                              datos_conexion=config.get('datos_conexion'),
                              lista_especiales=config.get('lista_especiales'),
                              excel= excel
                              )
        else:
            print(file)
            print("RESUMEN: El archivo indicado no es un directorio")
    print("Tiempo el análisis es: ", carpeta, time.time() - start)


def ejecutar_por_lote(carpeta_p, config, production):
    start = time.time()
    for f in os.listdir(carpeta_p):
        file = carpeta_p + f + '/root/'
        fecha_eval = f.split('LOGS-')[1].replace('-', '/')
        if os.path.isdir(file):
            porOficina(file, fecha_eval, config, production)
        else:
            print(file)
            print("RESUMEN: El archivo indicado no es un directorio")

    print("Tiempo General es: ", time.time() - start)


if __name__ == '__main__':

    # Parametros iniciales de archivo 'config'
    config = util.iniConfig()
    # Num param
    if len(sys.argv) <= 3:
        # carpeta = 'LOGS-s0144001/'  # sys.argv[1]

        carpeta_p = 'logsTest/test/'
        #fecha_eval = sys.argv[1]  # '23/2/2019'  # sys.argv[2]
        fecha_eval = '28/2/2019'
        #carpeta_p = 'logsProd/logs/LOGS-' + fecha_eval.replace('/', '-') + '/root/'
        production = False
        excel = False

        # ejecutar_por_lote(carpeta_p, config, production)
        porOficina(carpeta_p, fecha_eval, config, production,excel)
