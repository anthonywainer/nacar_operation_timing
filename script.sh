#!/usr/bin/env bash
#Variables generales
RUTA_SCRIPT=$(pwd)
RUTA_LOGS=/home/bash/Escritorio/worklogtiming/servidor/
RUTA_BACKUP=/home/bash/Escritorio/worklogtiming/backup/
FECHA_ANALIZAR=27/11/2018

TMP=resultado.txt

HORA_INI=$(date +"%H:%M:%S.%3N")

echo "Obteniendo archivos comprimidos..."
archivos_comprimidos=($(/usr/bin/find $RUTA_LOGS -name '*.tar.gz'))

echo "Cambiando a entorno virtualizado..."
source venv/bin/activate

#Recorriendo lista de archivos comprimidos
for archivo in ${archivos_comprimidos[*]}
do
    cd $RUTA_LOGS
    echo "---------------------------------------------------"
    echo "Descomprimir archivo " $archivo
    tar -xzvf $archivo >> $TMP
    echo "Moviendo archivo " $archivo " a directorio de backup"
    mv $archivo $RUTA_BACKUP

    #Ejecutar script python
    cd $RUTA_SCRIPT
    SUBRUTA_FOLDER=${archivo::-7}
    echo "Ejecutando script en " $SUBRUTA_FOLDER
    python main.py $SUBRUTA_FOLDER $FECHA_ANALIZAR

    echo "Eliminando carpeta analizada" $SUBRUTA_FOLDER
    rm -rf $SUBRUTA_FOLDER
    echo "---------------------------------------------------"
done

#Resultado de descomprimir
cd $RUTA_LOGS
if [ -f $TMP ]; then
    mv $TMP $RUTA_BACKUP
fi

HORA_FIN=$(date +"%H:%M:%S.%3N")

echo "HORA INICIO: " $HORA_INI
echo "HORA FIN: " $HORA_FIN
echo "Finalizó ejecución de script"