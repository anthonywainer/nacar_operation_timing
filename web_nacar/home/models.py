from django.db import models
from django.db.models import (Avg, Max, Min, Sum, Count)
import numpy as np


# Create your models here.
class Test(models.Model):
    d_a = models.DurationField(db_column='duracion_a')
    d_b = models.DurationField(db_column='duracion_b')
    d_c = models.DurationField(db_column='duracion_c')


class Territorio(models.Model):
    class Meta:
        db_table = 'territorio'
    territorio = models.CharField(max_length=200, db_column='territorio')


class Oficinas(models.Model):
    class Meta:
        db_table = 'oficinas'
    codigo_oficina = models.CharField(max_length=4, db_column='codigo_oficina')
    oficina = models.CharField(max_length=200, db_column='oficina')
    territorio = models.ForeignKey(Territorio, db_column='territorio', on_delete=True)


class DetalleOperation(models.Model):
    class Meta:
        db_table = 'operation_timing_detalle'

    estado = models.BooleanField(null=False, db_column='estado')
    identificador = models.CharField(max_length=100, db_column='identificador')
    codigo_oficina = models.CharField(max_length=4, db_column='codigo_oficina')
    fecha = models.DateField(db_column='fecha')
    iter_oficina = models.PositiveIntegerField(db_column='iter_oficina')
    puesto = models.CharField(max_length=4, db_column='puesto')
    iter_puesto = models.PositiveIntegerField(db_column='iter_puesto')
    codigo_escenario = models.CharField(max_length=20, db_column='codigo_escenario')
    nombre_operativa = models.CharField(max_length=100, db_column='nombre_operativa')  #
    codigo_operativa = models.CharField(max_length=20, db_column='codigo_operativa')
    tiempo_operativa = models.DurationField(db_column='tiempo_operativa')
    tiempo_parcial = models.DurationField(db_column='tiempo_parcial', null=True)  #
    hora_inicio = models.DurationField(db_column='hora_inicio', null=True)
    hora_fin = models.DurationField(db_column='hora_fin', null=True)
    hora_palabra_reservada = models.DurationField(db_column='hora_palabra_reservada', null=True)  #
    codigo_ventana_tx = models.CharField(max_length=20, db_column='codigo_ventana_tx')  #
    detalle_ventana_tx = models.CharField(max_length=200, db_column='detalle_ventana_tx', null=True)  #
    iter_tramo = models.PositiveIntegerField(db_column='iter_tramo')  #
    numero_tramos = models.PositiveIntegerField(db_column='numero_tramos')
    nombre_flujo = models.CharField(max_length=300, db_column='nombre_flujo')
    iter_flujo_puesto = models.PositiveIntegerField(db_column='iter_flujo_puesto')
    iter_flujo_oficina = models.PositiveIntegerField(db_column='iter_flujo_oficina')
    #hora_escenario = models.DurationField(db_column='hora_escenario', null=True)
    #terminal = models.CharField(db_column='terminal', null=True, max_length=4)
    #usuario = models.CharField(db_column='usuario', null=True,max_length=8)
    #hora_ini_escenario = models.DurationField(db_column='hora_ini_escenario', null=True)
    #hora_ini_operativa = models.DurationField(db_column='hora_ini_operativa', null=True)

    def __str__(self):
        return self.codigo_ventana_tx + " - " + str(self.hora_palabra_reservada)

    pass


class ResumenOperation(models.Model):
    class Meta:
        db_table = 'operation_timing_resumen'

    estado = models.BooleanField(null=False, db_column='estado')
    identificador = models.CharField(max_length=100, db_column='identificador')
    fecha = models.DateField(db_column='fecha')
    puesto = models.CharField(max_length=4, db_column='puesto')
    codigo_oficina = models.CharField(max_length=4, db_column='codigo_oficina')
    iter_oficina = models.PositiveIntegerField(db_column='iter_oficina')
    nombre_operativa = models.CharField(max_length=100, db_column='nombre_operativa')
    iter_puesto = models.PositiveIntegerField(db_column='iter_puesto')
    hora_inicio = models.DurationField(db_column='hora_inicio')
    hora_fin = models.DurationField(db_column='hora_fin')
    codigo_escenario = models.CharField(max_length=20, db_column='codigo_escenario')
    codigo_operativa = models.CharField(max_length=20, db_column='codigo_operativa')
    tiempo_operativa = models.DurationField(db_column='tiempo_operativa')
    tiempo_sistema = models.DurationField(db_column='tiempo_sistema')
    tiempo_disponible = models.DurationField(db_column='tiempo_disponible')
    tiempo_usuario = models.DurationField(db_column='tiempo_usuario')
    tiempo_disponible_neto = models.DurationField(db_column='tiempo_disponible_neto')
    tiempo_disponible_real = models.DurationField(db_column='tiempo_disponible_real')
    tiempo_disponible_real_a = models.DurationField(db_column='tiempo_disponible_real_a')
    tiempo_disponible_real_b = models.DurationField(db_column='tiempo_disponible_real_b')
    numero_tramos = models.PositiveIntegerField(db_column='numero_tramos')
    iter_flujo_puesto = models.PositiveIntegerField(db_column='iter_flujo_puesto')
    iter_flujo_oficina = models.PositiveIntegerField(db_column='iter_flujo_oficina')
    nombre_flujo = models.CharField(max_length=300, db_column='nombre_flujo')
    hora_escenario = models.DurationField(db_column='hora_escenario', null=True)
    terminal = models.CharField(db_column='terminal', null=True, max_length=4)
    usuario = models.CharField(db_column='usuario', null=True,max_length=8)
    hora_ini_escenario = models.DurationField(db_column='hora_ini_escenario', null=True)
    hora_ini_operativa = models.DurationField(db_column='hora_ini_operativa', null=True)
    hora_fin_escenario = models.DurationField(db_column='hora_fin_escenario', null=True)
    hora_fin_operativa = models.DurationField(db_column='hora_fin_operativa', null=True)
    hora_operativa = models.DurationField(db_column='hora_operativa', null=True)

    def __str__(self):
        return self.identificador

    def getNameOficina(self):
        return Oficinas.objects.filter(codigo_oficina=self.codigo_oficina).values()

    def getNameOficinab(cod):
        return Oficinas.objects.filter(codigo_oficina=cod).values()

import datetime

class Utilitarios:

    def getDatosxFlujo(self, datos_flujo):
        """
        :rtype: object
        """
        response = []
        for c in self:
            # Percentil
            tmp_valores = datos_flujo.values_list(c)

            tmp = [[t[0]] for t in tmp_valores]

            response.append({
                # 'count':datos_flujo.aggregate(Count(c)).get(c+'__count'),
                'columna': c,
                'percentile': str(np.percentile(np.array(tmp), 90)).split(".")[0],
                'avg': str(datos_flujo.aggregate(Avg(c)).get(c + '__avg')).split(".")[0],
                'min': str(datos_flujo.aggregate(Min(c)).get(c + '__min')).split(".")[0],
                'max': str(datos_flujo.aggregate(Max(c)).get(c + '__max')).split(".")[0]
            })

        return response

    pass
