# Generated by Django 2.1.5 on 2019-03-31 17:15

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('home', '0013_auto_20190325_1047'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='detalleoperation',
            name='hora_escenario',
        ),
        migrations.RemoveField(
            model_name='detalleoperation',
            name='hora_ini_escenario',
        ),
        migrations.RemoveField(
            model_name='detalleoperation',
            name='hora_ini_operativa',
        ),
        migrations.RemoveField(
            model_name='detalleoperation',
            name='terminal',
        ),
        migrations.RemoveField(
            model_name='detalleoperation',
            name='usuario',
        ),
        migrations.AddField(
            model_name='resumenoperation',
            name='hora_fin_escenario',
            field=models.DurationField(db_column='hora_fin_escenario', null=True),
        ),
        migrations.AddField(
            model_name='resumenoperation',
            name='hora_fin_operativa',
            field=models.DurationField(db_column='hora_fin_operativa', null=True),
        ),
        migrations.AddField(
            model_name='resumenoperation',
            name='hora_operativa',
            field=models.DurationField(db_column='hora_operativa', null=True),
        ),
    ]
