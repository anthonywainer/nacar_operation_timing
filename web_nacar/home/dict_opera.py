dict_operativas =  {
    'DEPOSITO EN EFECTIVO' : {
        'Depósito Cheques': 'Normal',
        'Depósito Cheques con Lineas a Pantalla': 'Normal',
        'Depósito Cheques con Tx Significativas': 'Tx Significativas',
        'Depósito Cheques con Tx Significativas con Lineas a Pantalla': 'Tx Significativas',
        'Depósito Cheques con Tx Significativas con Lineas a Pantalla Impresion': 'Tx Sig. Impresion',
        'Depósito Cheques con Tx Significativas con Lineas a Pantalla Autorización': 'Tx Sig. Autorización',
        'Depósito Cheques con Tx Significativas con Lineas a Pantalla Impresion Autorización': 'Tx Sig. Impresion '
                                                                                               'Autorización',
        'Depósito Cheques con Tx Significativas con Lineas a Pantalla Autorización Impresion': 'Tx Sig. Autorización '
                                                                                               'Impresion',

        'Depósito Cheques con Tx Significativas Impresion': 'Tx Sig. Impresion',
        'Depósito Cheques con Tx Significativas Autorización': 'Tx Sig. Autorización',
        'Depósito Cheques con Tx Significativas Impresion Autorización': 'Tx Sig. Impresion Autorización',
        'Depósito Cheques con Tx Significativas Autorización Impresion': 'Tx Sig. Autorización Impresion',


        'Depósito con Lineas a Pantalla': 'Normal',
        'Depósito con Lineas a Pantalla con Tx Significativas Impresion': 'Tx Sig. Impresion',
        'Depósito con Lineas a Pantalla con Tx Significativas Autorización': 'Tx Sig. Autorización',
        'Depósito con Lineas a Pantalla con Tx Significativas Impresion Autorización': 'Tx Sig. Impresion Autorización',
        'Depósito con Lineas a Pantalla con Tx Significativas Autorización Impresion': 'Tx Sig. Autorización Impresion',

        'Depósito Cuenta Recaudadora': 'Normal',
        'Depósito Cuenta Recaudadora con Tx Significativas Impresion': 'Tx Sig. Impresion',
        'Depósito Cuenta Recaudadora con Tx Significativas Autorización': 'Tx Sig. Autorización',
        'Depósito Cuenta Recaudadora con Tx Significativas Impresion Autorización': 'Tx Sig. Impresion Autorización',
        'Depósito Cuenta Recaudadora con Tx Significativas Autorización Impresion': 'Tx Sig. Autorización Impresion',

        'Depósito Especial': 'Normal',
        'Depósito Especial con Tx Significativas Impresion': 'Tx Sig. Impresion',
        'Depósito Especial con Tx Significativas Autorización': 'Tx Sig. Autorización',
        'Depósito Especial con Tx Significativas Impresion Autorización': 'Tx Sig. Impresion Autorización',
        'Depósito Especial con Tx Significativas Autorización Impresion': 'Tx Sig. Autorización Impresion',


        'Depósito Normal + Ayuda': 'Normal',
        'Depósito Normal con Tx Significativas Impresion': 'Tx Sig. Impresion',
        'Depósito Normal con Tx Significativas Autorización': 'Tx Sig. Autorización',
        'Depósito Normal con Tx Significativas Impresion Autorización': 'Tx Sig. Impresion Autorización',
        'Depósito Normal con Tx Significativas Autorización Impresion': 'Tx Sig. Autorización Impresion',

        'Depósito con N° de Cuenta': 'Normal',
        'Depósito con N° de Cuenta con Tx Significativas Impresion': 'Tx Sig. Impresion',
        'Depósito con N° de Cuenta con Tx Significativas Autorización': 'Tx Sig. Autorización',
        'Depósito con N° de Cuenta con Tx Significativas Impresion Autorización': 'Tx Sig. Impresion Autorización',
        'Depósito con N° de Cuenta con Tx Significativas Autorización Impresion': 'Tx Sig. Autorización Impresion',

        'Depósito con mod. de datos': 'Normal',
        'Depósito con mod. de datos con Tx Significativas Impresion': 'Tx Sig. Impresion',
        'Depósito con mod. de datos con Tx Significativas Autorización': 'Tx Sig. Autorización',
        'Depósito con mod. de datos con Tx Significativas Impresion Autorización': 'Tx Sig. Impresion Autorización',
        'Depósito con mod. de datos con Tx Significativas Autorización Impresion': 'Tx Sig. Autorización Impresion',
    },

    'RETIRO DE EFECTIVO':{
        'Retiro con N° de Cuenta': 'Normal',
        'Retiro con N° de Cuenta + Tx Significativas': 'Tx Significativas',
        'Retiro con N° de Cuenta + Tx Significativas Impresion': 'Tx Sig. Impresion',
        'Retiro con N° de Cuenta + Tx Significativas Autorización': 'Tx Sig. Autorización',
        'Retiro con N° de Cuenta + Tx Significativas Impresion Autorización': 'Tx Sig. Impresion Autorización',
        'Retiro con N° de Cuenta + Tx Significativas Autorización Impresion': 'Tx Sig. Autorización Impresion',
    },
    'OPERACION CON DNI': {
        'Depósito con DNI': 'Normal',
        'Depósito con DNI + Tx Significativas': 'Tx Significativas',
        'Depósito con DNI + Tx Significativas Impresion': 'Tx Sig. Impresion',
        'Depósito con DNI + Tx Significativas Autorización': 'Tx Sig. Autorización',
        'Depósito con DNI + Tx Significativas Impresion Autorización': 'Tx Sig. Impresion Autorización',
        'Depósito con DNI + Tx Significativas Autorización Impresion': 'Tx Sig. Autorización Impresion',

        'Retiro con DNI': 'Normal',
        'Retiro con DNI + Tx Significativas': 'Tx Significativas',
        'Retiro con DNI + Tx Significativas Impresion': 'Tx Sig. Impresion',
        'Retiro con DNI + Tx Significativas Autorización': 'Tx Sig. Autorización',
        'Retiro con DNI + Tx Significativas Impresion Autorización': 'Tx Sig. Impresion Autorización',
        'Retiro con DNI + Tx Significativas Autorización Impresion': 'Tx Sig. Autorización Impresion',
    },

    'OPERACION CON LECTURA DE TARJETA': {
        'Depósito con lectura de tarjeta': 'Normal',
        'Depósito con lectura de tarjeta + Tx Significativas': 'Tx Significativas',
        'Depósito con lectura de tarjeta + Tx Significativas Impresion': 'Tx Sig. Impresion',
        'Depósito con lectura de tarjeta + Tx Significativas Autorización': 'Tx Sig. Autorización',
        'Depósito con lectura de tarjeta + Tx Significativas Impresion Autorización': 'Tx Sig. Impresion Autorización',
        'Depósito con lectura de tarjeta + Tx Significativas Autorización Impresion': 'Tx Sig. Autorización Impresion',

        'Retiro con lectura de tarjeta': 'Normal',
        'Retiro con lectura de tarjeta + Tx Significativas': 'Tx Significativas',
        'Retiro con lectura de tarjeta + Tx Significativas Impresion': 'Tx Sig. Impresion',
        'Retiro con lectura de tarjeta + Tx Significativas Autorización': 'Tx Sig. Autorización',
        'Retiro con lectura de tarjeta + Tx Significativas Impresion Autorización': 'Tx Sig. Impresion Autorización',
        'Retiro con lectura de tarjeta + Tx Significativas Autorización Impresion': 'Tx Sig. Autorización Impresion',

    }
}