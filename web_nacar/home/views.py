from datetime import datetime

import numpy as np
import pandas as pd
from django.db.models import Count, Avg
from django.db.models.expressions import RawSQL
from django.shortcuts import render
from .models import ResumenOperation
from .models import DetalleOperation
from .models import Utilitarios
from .models import Oficinas
# import locale
# locale.setlocale(locale.LC_ALL, 'es_Es')
from django.utils import translation
from django.utils.translation import ugettext as _
import calendar
import time


class RawAnnotation(RawSQL):
    """
    RawSQL also aggregates the SQL to the `group by` clause which defeats the purpose of adding it to an Annotation.
    """

    def get_group_by_cols(self):
        return []


# Create your views here.
def operativas_oficinas(request):
    resumen_operativas = ResumenOperation.objects.order_by('tiempo_operativa')

    return render(request, "home/operativas_oficinas.html", {'resumen_operativas': resumen_operativas})


def index_reportes(request):
    resumen = ResumenOperation.objects.values('codigo_oficina').annotate(dcount=Count('codigo_oficina'))
    correctos = resumen.filter(estado=True)
    incorrectos = resumen.filter(estado=False)

    resumen = []

    flujos = {c["codigo_oficina"]: {'cant_c': c["dcount"]} for c in correctos}

    for i in incorrectos:
        flujos[i["codigo_oficina"]]['cant_i'] = i["dcount"]

    for o, f in flujos.items():
        o = [[i['codigo_oficina'], i['oficina']] for i in ResumenOperation.getNameOficinab(o)][0]

        if 'cant_i' in f.keys():
            ci = 0
        else:
            ci = f['cant_i']
        resumen.append({'oficina': o, 'correctos': f['cant_c'], 'incorrectos': ci})

    return render(request, 'home/index.html', {'resumen': resumen})


def get_list_resumen_operativas(request):
    set_list_nombre_flujos = ResumenOperation.objects.values('nombre_flujo').distinct().filter(estado='True')

    # Filtros
    codigo_operativa = request.GET.get('codigo_operativa', '')
    fecha = request.GET.get('fecha', '')
    if codigo_operativa != '':
        set_list_nombre_flujos = set_list_nombre_flujos.filter(codigo_operativa=codigo_operativa)
    if fecha != '':
        set_list_nombre_flujos = set_list_nombre_flujos.filter(fecha=fecha)

    # Datos
    lista_x_flujos_resumen_operativas = []
    nombres = [n.get('nombre_flujo') for n in set_list_nombre_flujos]
    nombres = sorted(nombres)
    for i in range(0, len(nombres)):
        datos_x_flujo = set_list_nombre_flujos.filter(nombre_flujo=nombres[i]).values().order_by('tiempo_operativa')
        # Calcular
        response_estadistico = Utilitarios.getDatosxFlujo(datos_flujo=datos_x_flujo,
                                                          self=('tiempo_operativa', 'tiempo_sistema',
                                                                'tiempo_disponible', 'tiempo_usuario'))
        # Añadir
        lista_x_flujos_resumen_operativas.append({
            'indice': i + 1,
            'nombre_flujo': nombres[i].replace('/', '_'),
            'datos': datos_x_flujo,
            'datos_calculados': response_estadistico,
        })

    return render(request, 'home/resumen_operativas.html',
                  {'resumen_operativas_xflujos': lista_x_flujos_resumen_operativas, 'rango': range(4)})


# list de tiempos
def get_list_subflujo_oficina(request, oficina):
    oficina = Oficinas.objects.filter(codigo_oficina=oficina)

    return render(request, 'home/Lista_tiempos.html', {'oficina': oficina})


def reporte(request):
    return render(request, 'home/reporte.html')


def reporte2(request):
    return render(request, 'home/reporte2_i.html')


# list de tiempos, oficina
dict_operativas = {
    'DEPOSITO EN EFECTIVO': {
        'Depósito Especial': 'Normal',
        'Depósito Especial + Tx Significativas': 'Tx Significativas',
        'Depósito Especial + Tx Significativas Impresion': 'Tx Sig. Impresion',
        'Depósito Especial + Tx Significativas Autorización': 'Tx Sig. Autorización',
        'Depósito Especial + Tx Significativas Impresion Autorización': 'Tx Sig. Impresion Autorización',
        'Depósito Especial + Tx Significativas Autorización Impresion': 'Tx Sig. Autorización Impresion',

        'Depósito con N° de Cuenta': 'Normal',
        'Depósito con N° de Cuenta + Tx Significativas': 'Tx Significativas',
        'Depósito con N° de Cuenta + Tx Significativas Impresion': 'Tx Sig. Impresion',
        'Depósito con N° de Cuenta + Tx Significativas Autorización': 'Tx Sig. Autorización',
        'Depósito con N° de Cuenta + Tx Significativas Impresion Autorización': 'Tx Sig. Impresion Autorización',
        'Depósito con N° de Cuenta + Tx Significativas Autorización Impresion': 'Tx Sig. Autorización Impresion',

        'Depósito con N° de Cuenta con mod. de datos': 'Normal',
        'Depósito con N° de Cuenta con mod. de datos Tx Significativas Impresion': 'Tx Sig. Impresion',
        'Depósito con N° de Cuenta con mod. de datos Tx Significativas datos Autorización': 'Tx Sig. Autorización',
        'Depósito con N° de Cuenta con mod. de datos Tx Significativas datos Impresion Autorización': 'Tx Sig. Impresion Autorización',
        'Depósito con N° de Cuenta con mod. de datos Tx Significativas datos Autorización Impresion': 'Tx Sig. Autorización Impresion',

    },
    'DEPÓSITO CON CHEQUES': {
        'Depósito Cheques': 'Normal',
        'Depósito Cheques con Tx Significativas': 'Tx Significativas',
        'Depósito Cheques con Tx Significativas Impresion': 'Tx Sig. Impresion',
        'Depósito Cheques con Tx Significativas Autorización': 'Tx Sig. Autorización',
        'Depósito Cheques con Tx Significativas Impresion Autorización': 'Tx Sig. Impresion Autorización',
        'Depósito Cheques con Tx Significativas Autorización Impresion': 'Tx Sig. Autorización Impresion',

        'Depósito Cheques Especial': 'Normal',
        'Depósito Cheques Especial + Tx Significativas': 'Tx Significativas',
        'Depósito Cheques Especial + Tx Significativas Impresion': 'Tx Sig. Impresion',
        'Depósito Cheques Especial + Tx Significativas Autorización': 'Tx Sig. Autorización',
        'Depósito Cheques Especial + Tx Significativas Autorización Impresion': 'Tx Sig. Impresion Autorización',
        'Depósito Cheques Especial + Tx Significativas Impresion Autorización': 'Tx Sig. Autorización Impresion',
    },

    'RETIRO DE EFECTIVO': {
        'Retiro con N° de Cuenta': 'Normal',
        'Retiro con N° de Cuenta + Tx Significativas': 'Tx Significativas',
        'Retiro con N° de Cuenta + Tx Significativas Impresion': 'Tx Sig. Impresion',
        'Retiro con N° de Cuenta + Tx Significativas Autorización': 'Tx Sig. Autorización',
        'Retiro con N° de Cuenta + Tx Significativas Impresion Autorización': 'Tx Sig. Impresion Autorización',
        'Retiro con N° de Cuenta + Tx Significativas Autorización Impresion': 'Tx Sig. Autorización Impresion',
    },
    'OPERACION CON DNI': {
        'Depósito con DNI': 'Normal',
        'Depósito con DNI + Tx Significativas': 'Tx Significativas',
        'Depósito con DNI + Tx Significativas Impresion': 'Tx Sig. Impresion',
        'Depósito con DNI + Tx Significativas Autorización': 'Tx Sig. Autorización',
        'Depósito con DNI + Tx Significativas Impresion Autorización': 'Tx Sig. Impresion Autorización',
        'Depósito con DNI + Tx Significativas Autorización Impresion': 'Tx Sig. Autorización Impresion',

        'Retiro con DNI': 'Normal',
        'Retiro con DNI + Tx Significativas': 'Tx Significativas',
        'Retiro con DNI + Tx Significativas Impresion': 'Tx Sig. Impresion',
        'Retiro con DNI + Tx Significativas Autorización': 'Tx Sig. Autorización',
        'Retiro con DNI + Tx Significativas Impresion Autorización': 'Tx Sig. Impresion Autorización',
        'Retiro con DNI + Tx Significativas Autorización Impresion': 'Tx Sig. Autorización Impresion',
    },

    'OPERACION CON LECTURA DE TARJETA': {
        'Depósito con lectura de tarjeta': 'Normal',
        'Depósito con lectura de tarjeta + Tx Significativas': 'Tx Significativas',
        'Depósito con lectura de tarjeta + Tx Significativas Impresion': 'Tx Sig. Impresion',
        'Depósito con lectura de tarjeta + Tx Significativas Autorización': 'Tx Sig. Autorización',
        'Depósito con lectura de tarjeta + Tx Significativas Impresion Autorización': 'Tx Sig. Impresion Autorización',
        'Depósito con lectura de tarjeta + Tx Significativas Autorización Impresion': 'Tx Sig. Autorización Impresion',

        'Retiro con lectura de tarjeta': 'Normal',
        'Retiro con lectura de tarjeta + Tx Significativas': 'Tx Significativas',
        'Retiro con lectura de tarjeta + Tx Significativas Impresion': 'Tx Sig. Impresion',
        'Retiro con lectura de tarjeta + Tx Significativas Autorización': 'Tx Sig. Autorización',
        'Retiro con lectura de tarjeta + Tx Significativas Impresion Autorización': 'Tx Sig. Impresion Autorización',
        'Retiro con lectura de tarjeta + Tx Significativas Autorización Impresion': 'Tx Sig. Autorización Impresion',

    },
    'RECAUDOS Y PAGO DE SERVICIOS': {
        'Recaudos y pago de Servicios': 'Normal',
        'Recaudos y pago de Servicios + Tx Significativas': 'Tx Significativas',
        'Recaudos y pago de Servicios + Tx Significativas Impresion': 'Tx Sig. Impresion',
        'Recaudos y pago de Servicios + Tx Significativas Autorización': 'Tx Sig. Autorización',
        'Recaudos y pago de Servicios + Tx Significativas Impresion Autorización': 'Tx Sig. Impresion Autorización',
        'Recaudos y pago de Servicios + Tx Significativas Autorización Impresion': 'Tx Sig. Autorización Impresion',

        'Recaudo con Búsqueda Alfabética de Cliente': 'Normal',
        'Recaudo con Búsqueda Alfabética de Cliente + Tx Significativas': 'Tx Significativas',
        'Recaudo con Búsqueda Alfabética de Cliente + Tx Significativas Impresion': 'Tx Sig. Impresion',
        'Recaudo con Búsqueda Alfabética de Cliente + Tx Significativas Autorización': 'Tx Sig. Autorización',
        'Recaudo con Búsqueda Alfabética de Cliente + Tx Significativas Impresion Autorización': 'Tx Sig. Impresion '
                                                                                                 'Autorización',
        'Recaudo con Búsqueda Alfabética de Cliente + Tx Significativas Autorización Impresion': 'Tx Sig. '
                                                                                                 'Autorización '
                                                                                                 'Impresion',
    }
}
columns = ['Normal', 'Tx Significativas', 'Tx Sig. Impresion',
           'Tx Sig. Autorización', 'Tx Sig. Autorización Impresion']
dic_operativa_cuenta = [
    'DEPOSITO EN EFECTIVO',
    'RETIRO DE EFECTIVO',
    'RECAUDOS Y PAGO DE SERVICIOS'
]


def clasi_opera():
    nco = {}
    for k, v in dict_operativas.items():
        nco[k] = {}
        for i, j in v.items():
            if j in columns:
                if j in nco[k].keys():
                    nco[k][j].append(i)
                else:
                    nco[k][j] = [i]
    return nco


def dashboard(request):
    tipo_tiempo = request.GET.get('tipo_tiempo', 'tiempo_operativa')

    # sub_operativas = ['DNI', 'CUENTA', 'TARJETA']

    new_dict_operativa = {}
    estado = request.GET.get('estado', True)

    oficina = request.GET.get('oficina', '')
    anio = request.GET.get('anio', '')
    mes = request.GET.get('mes', '')
    dia = request.GET.get('dia', '')

    reop = ResumenOperation.objects
    kwargs_filter = dict(estado=estado)
    if oficina:
        if not oficina == '0':
            kwargs_filter['codigo_oficina'] = oficina
    if anio:
        if not anio == '0':
            kwargs_filter['fecha__year'] = anio
    if mes:
        if not mes == '0':
            kwargs_filter['fecha__month'] = mes
    if dia:
        if not dia == '0':
            if dia == '1':
                kwargs_filter['fecha__day__lte'] = 15
            else:
                kwargs_filter['fecha__day__gte'] = 15

    oficinas = reop.values_list('codigo_oficina').distinct().order_by('codigo_oficina').filter(estado=estado)
    oficinas = list(Oficinas.objects.filter(codigo_oficina__in=oficinas).values().order_by('oficina'))
    oficinas = [{'codigo_oficina': '0', 'oficina': 'Todos'}] + oficinas
    anios = [(an[0], an[0]) for an in reop.values_list('fecha__year').distinct().order_by('fecha__year')]

    if len(anios):
        anio = anios[0][0]

    meses = ''
    translation.activate('es')
    nombre_meses = calendar.month_name
    nombre_dia_abre = calendar.day_abbr
    nombre_dias = calendar.day_name
    semaforo = {
        'green': ['00:00:00', '00:02:30'],
        'yellow': ['00:02:31', '00:05:26'],
        'red': ['00:05:27', '00:40:26'],
    }

    if anio:
        querymes = reop.filter(fecha__year=anio, estado=estado)
        meses = querymes.values_list('fecha__month').distinct().order_by('fecha__month')
        meses = [{'idmes': m[0], 'mes': _(nombre_meses[m[0]])} for m in meses]
        meses_list = meses

        if mes:
            if not mes == '0':
                meses = [{'idmes': int(mes), 'mes': _(nombre_meses[int(mes)])}]

        for m in meses:
            mes = m['idmes']
            dias = querymes.filter(fecha__month=mes).values_list('fecha__day',
                                                                 'fecha__week_day').distinct().order_by('fecha__day')
            m['dias'] = [{'iddia': d[0], 'dia_abr': _(nombre_dia_abre[d[1] - 2]), 'dia': _(nombre_dias[d[1] - 2])}
                         for d in dias]
            m['cant'] = len(m['dias'])

    reop = reop.filter(**kwargs_filter)
    dico = clasi_opera()
    for no in dic_operativa_cuenta:
        if reop.filter(nombre_operativa=no):
            if no == 'DEPOSITO EN EFECTIVO':
                for k, v in dico['DEPÓSITO CON CHEQUES'].items():
                    noflu = reop.filter(nombre_flujo__in=v). \
                        values('fecha').annotate(
                        avg_duration=Avg(tipo_tiempo),
                        perc_90_duration=RawAnnotation('percentile_disc(%s) WITHIN GROUP (ORDER BY '+tipo_tiempo+')',
                                                       (0.9,)),
                    ).order_by('fecha')

                    if noflu:
                        nob = 'DEPÓSITO CON CHEQUES'
                        if not nob in new_dict_operativa.keys():
                            new_dict_operativa[nob] = {}

                        tmp = [[t[0], t[1]] for t in noflu.values_list('fecha', 'perc_90_duration')]
                        if not k in new_dict_operativa[nob].keys():
                            new_dict_operativa[nob][k] = [tmp]
                        else:
                            new_dict_operativa[nob][k].append(tmp)

            if not no in new_dict_operativa.keys():
                new_dict_operativa[no] = {}

            for k, v in dico[no].items():
                # print(k,' -> ', v)
                noflu = reop.filter(nombre_flujo__in=v). \
                    values('fecha').annotate(
                    avg_duration=Avg(tipo_tiempo),
                    perc_90_duration=RawAnnotation('percentile_disc(%s) WITHIN GROUP (ORDER BY '+tipo_tiempo+')',
                                                   (0.9,)),
                ).order_by('fecha')

                if noflu:
                    if not no in new_dict_operativa.keys():
                        new_dict_operativa[no] = {}

                    tmp = [[t[0], t[1]] for t in noflu.values_list('fecha', 'perc_90_duration')]
                    if not k in new_dict_operativa[no].keys():
                        new_dict_operativa[no][k] = [tmp]
                    else:
                        new_dict_operativa[no][k].append(tmp)

    no = 'OPERACION CON DNI'
    if reop.filter(nombre_operativa=no):
        for k, v in dico[no].items():
            noflu = reop.filter(nombre_flujo__in=v). \
                values('fecha').annotate(
                avg_duration=Avg(tipo_tiempo),
                perc_90_duration=RawAnnotation('percentile_disc(%s) WITHIN GROUP (ORDER BY '+tipo_tiempo+')',
                                               (0.9,)),
            ).order_by('fecha')

            if noflu:
                if k.split(' ')[0] == 'Retiro':
                    no = dic_operativa_cuenta[1]
                else:
                    no = dic_operativa_cuenta[0]

                if not no in new_dict_operativa.keys():
                    new_dict_operativa[no] = {}

                tmp = [[t[0], t[1]] for t in noflu.values_list('fecha', 'perc_90_duration')]
                if not k in new_dict_operativa[no].keys():
                    new_dict_operativa[no][k] = [tmp]
                else:
                    new_dict_operativa[no][k].append(tmp)

    no = 'OPERACION CON LECTURA DE TARJETA'
    if reop.filter(nombre_operativa=no):
        for k, v in dico[no].items():
            noflu = reop.filter(nombre_flujo__in=v). \
                values('fecha').annotate(
                avg_duration=Avg(tipo_tiempo),
                perc_90_duration=RawAnnotation('percentile_disc(%s) WITHIN GROUP (ORDER BY '+tipo_tiempo+')',
                                               (0.9,)),
            ).order_by('fecha')
            if noflu:
                if k.split(' ')[0] == 'Retiro':
                    no = dic_operativa_cuenta[1]
                else:
                    no = dic_operativa_cuenta[0]

                if not no in new_dict_operativa.keys():
                    new_dict_operativa[no] = {}

                tmp = [[t[0], t[1]] for t in noflu.values_list('fecha', 'perc_90_duration')]
                if not k in new_dict_operativa[no].keys():
                    new_dict_operativa[no][k] = [tmp]
                else:
                    new_dict_operativa[no][k].append(tmp)

    new_list = []
    lista_mases = []
    for me in meses:
        for dm in me['dias']:
            lista_mases.append([me['idmes'], dm['iddia']])

    for k, v in new_dict_operativa.items():
        cont = 0
        for i, j in v.items():

            dmm = {}
            for me in j[0]:
                month = me[0].month
                if not month in dmm.keys():
                    dmm[month] = {}

                day = me[0].day
                if not day in dmm[month].keys():
                    dmm[month][day] = [me[1]]
                else:
                    dmm[month][day].append(me[1])

            # for ll in lista_mases:
            nnndays = []
            for ll in lista_mases:
                try:
                    if ll[1] in dmm[ll[0]].keys():
                        nnndays.append(dmm[ll[0]][ll[1]][0])
                    else:
                        nnndays.append('-')
                except KeyError:
                    nnndays.append('-')
            tiempo = [i[1] for i in j[0]]
            df = pd.DataFrame(pd.to_timedelta(tiempo)).mean()
            p90 = str(df[0]).split('days ')[1].split('.')[0]
            p90p = time.strptime(p90, "%H:%M:%S")
            if p90 == '00:00:00':
                p90 = '-'
            color = ''
            for se in semaforo:
                ini = time.strptime(semaforo[se][0], "%H:%M:%S")
                fin = time.strptime(semaforo[se][1], "%H:%M:%S")

                if ini <= p90p <= fin:
                    color = se

            if cont == 1:
                k = ''
            new_list.append({
                'cant': len(v),
                'operativa': k,
                'tipo': i,
                'p90': p90,
                'by_dias': nnndays,
                'color': color
            })
            cont += 1
    anios = [(0, 'Todos')] + anios
    meses_list = [{'idmes': 0, 'mes': 'Todos'}] + meses_list
    dias_list = [(0, 'Todos')] + [(1, 'Primeros Quince'), (2, 'Últimos Quince')]

    return render(request, 'home/dashboard.html', {'oficinas': oficinas,
                                                   'anios': anios, 'meses': meses,
                                                   'new_list': new_list,
                                                   'meses_list': meses_list,
                                                   'dias_list': dias_list
                                                   })


def dashboard_indicador(request):
    tipo_tiempo = 'tiempo_operativa'
    sub_operativas = ['DNI', 'CUENTA', 'TARJETA']
    new_dict_operativa = {}
    estado = request.GET.get('estado', True)
    oficina = request.GET.get('oficina', '')
    anio = request.GET.get('anio', '')
    mes = request.GET.get('mes', '')
    dia = request.GET.get('dia', '')
    operativa = request.GET.get('operativa', '')
    indicador = request.GET.get('indicador', '')

    reop = ResumenOperation.objects
    kwargs_filter = dict(estado=estado)
    if oficina:
        if not oficina == '0':
            kwargs_filter['codigo_oficina'] = oficina
    if anio:
        if not anio == '0':
            kwargs_filter['fecha__year'] = anio
    if mes:
        if not mes == '0':
            kwargs_filter['fecha__month'] = mes
    if dia:
        if not dia == '0':
            if dia == '1':
                kwargs_filter['fecha__day__lte'] = 15
            else:
                kwargs_filter['fecha__day__gte'] = 15

    reop = reop.filter(**kwargs_filter)
    dico = clasi_opera()
    for no in dic_operativa_cuenta:
        if reop.filter(nombre_operativa=no):
            if no == 'DEPOSITO EN EFECTIVO':
                for k, v in dico['DEPÓSITO CON CHEQUES'].items():
                    noflu = reop.filter(nombre_flujo__in=v). \
                        values('fecha', 'nombre_flujo').annotate(
                        avg_duration=Avg('tiempo_operativa'),
                        cantidad_flujos=Count('nombre_flujo'),
                        perc_90_duration=RawAnnotation('percentile_disc(%s) WITHIN GROUP (ORDER BY tiempo_operativa)',
                                                       (0.9,)),
                    ).order_by('fecha')

                    if noflu:
                        nob = 'DEPÓSITO CON CHEQUES'
                        if not nob in new_dict_operativa.keys():
                            new_dict_operativa[nob] = {}

                        tmp = [[t[0], t[1], t[2], t[3]] for t in noflu.values_list('fecha', 'perc_90_duration',
                                                                                   'cantidad_flujos', 'nombre_flujo')]

                        if not k in new_dict_operativa[nob].keys():
                            new_dict_operativa[nob][k] = [tmp]
                        else:
                            new_dict_operativa[nob][k].append(tmp)

            if not no in new_dict_operativa.keys():
                new_dict_operativa[no] = {}

            for k, v in dico[no].items():
                # print(k,' -> ', v)
                noflu = reop.filter(nombre_flujo__in=v). \
                    values('fecha', 'nombre_flujo').annotate(
                    avg_duration=Avg('tiempo_operativa'),
                    cantidad_flujos=Count('nombre_flujo'),
                    perc_90_duration=RawAnnotation('percentile_disc(%s) WITHIN GROUP (ORDER BY tiempo_operativa)',
                                                   (0.9,)),
                ).order_by('fecha')

                if noflu:
                    if not no in new_dict_operativa.keys():
                        new_dict_operativa[no] = {}

                    tmp = [[t[0], t[1], t[2], t[3]] for t in noflu.values_list('fecha', 'perc_90_duration',
                                                                               'cantidad_flujos', 'nombre_flujo')]
                    if not k in new_dict_operativa[no].keys():
                        new_dict_operativa[no][k] = [tmp]
                    else:
                        new_dict_operativa[no][k].append(tmp)

    no = 'OPERACION CON DNI'
    if reop.filter(nombre_operativa=no):
        for k, v in dico[no].items():
            noflu = reop.filter(nombre_flujo__in=v). \
                values('fecha', 'nombre_flujo').annotate(
                avg_duration=Avg('tiempo_operativa'),
                cantidad_flujos=Count('nombre_flujo'),
                perc_90_duration=RawAnnotation('percentile_disc(%s) WITHIN GROUP (ORDER BY tiempo_operativa)',
                                               (0.9,)),
            ).order_by('fecha')

            if noflu:
                if k.split(' ')[0] == 'Retiro':
                    no = dic_operativa_cuenta[1]
                else:
                    no = dic_operativa_cuenta[0]

                if not no in new_dict_operativa.keys():
                    new_dict_operativa[no] = {}

                tmp = [[t[0], t[1], t[2], t[3]] for t in noflu.values_list('fecha', 'perc_90_duration',
                                                                           'cantidad_flujos', 'nombre_flujo')]
                if not k in new_dict_operativa[no].keys():
                    new_dict_operativa[no][k] = [tmp]
                else:
                    new_dict_operativa[no][k].append(tmp)

    no = 'OPERACION CON LECTURA DE TARJETA'
    if reop.filter(nombre_operativa=no):
        for k, v in dico[no].items():
            noflu = reop.filter(nombre_flujo__in=v). \
                values('fecha', 'nombre_flujo').annotate(
                avg_duration=Avg('tiempo_operativa'),
                cantidad_flujos=Count('nombre_flujo'),
                perc_90_duration=RawAnnotation('percentile_disc(%s) WITHIN GROUP (ORDER BY tiempo_operativa)',
                                               (0.9,)),
            ).order_by('fecha')
            if noflu:
                if k.split(' ')[0] == 'Retiro':
                    no = dic_operativa_cuenta[1]
                else:
                    no = dic_operativa_cuenta[0]

                if not no in new_dict_operativa.keys():
                    new_dict_operativa[no] = {}

                tmp = [[t[0], t[1], t[2], t[3]] for t in noflu.values_list('fecha', 'perc_90_duration',
                                                                           'cantidad_flujos', 'nombre_flujo')]
                if not k in new_dict_operativa[no].keys():
                    new_dict_operativa[no][k] = [tmp]
                else:
                    new_dict_operativa[no][k].append(tmp)

    print(new_dict_operativa)

    return render(request, 'home/reporte_operativas.html',
                  {'columnas': columns, 'flujos': '',
                   'tipo': tipo_tiempo.replace('_', ' de ')})


def get_list_oficina_tiempo(request, oficina, tipo_tiempo):
    columns = ['Normal', 'Tx Significativas', 'Tx Sig. Impresion',
               'Tx Sig. Autorización', 'Tx Sig. Autorización Impresion']
    dic_operativa_cuenta = [
        'DEPOSITO EN EFECTIVO',
        'RETIRO DE EFECTIVO',
        'RECAUDOS Y PAGO DE SERVICIOS'
    ]

    sub_operativas = ['DNI', 'CUENTA', 'TARJETA']

    new_dict_operativa = {}
    reop = ResumenOperation.objects.filter(codigo_oficina=oficina, estado=True)

    for no in dic_operativa_cuenta:
        if reop.filter(nombre_operativa=no):
            if no == 'DEPOSITO EN EFECTIVO':
                for k, v in dict_operativas['DEPÓSITO CON CHEQUES'].items():
                    noflu = reop.filter(nombre_flujo=k)
                    if noflu:
                        nob = 'DEPÓSITO CON CHEQUES'
                        if not nob in new_dict_operativa.keys():
                            new_dict_operativa[nob] = {}

                        if not sub_operativas[1] in new_dict_operativa[nob].keys():
                            new_dict_operativa[nob][sub_operativas[1]] = {}

                        if v in new_dict_operativa['DEPÓSITO CON CHEQUES'][sub_operativas[1]].keys():
                            tmp = [[t[0]] for t in noflu.values_list(tipo_tiempo)]
                            new_dict_operativa['DEPÓSITO CON CHEQUES'][sub_operativas[1]][v].append(
                                str(np.percentile(np.array(tmp), 90)).split(".")[0])
                        else:
                            tmp = [[t[0]] for t in noflu.values_list(tipo_tiempo)]
                            new_dict_operativa['DEPÓSITO CON CHEQUES'][sub_operativas[1]][v] = [
                                str(np.percentile(np.array(tmp), 90)).split(".")[0]]

            if not no in new_dict_operativa.keys():
                new_dict_operativa[no] = {}

            if not sub_operativas[1] in new_dict_operativa[no].keys():
                new_dict_operativa[no][sub_operativas[1]] = {}

            for k, v in dict_operativas[no].items():
                # print(k,' -> ', v)
                noflu = reop.filter(nombre_flujo=k)
                if noflu:
                    if v in new_dict_operativa[no][sub_operativas[1]].keys():
                        tmp = [[t[0]] for t in noflu.values_list(tipo_tiempo)]
                        new_dict_operativa[no][sub_operativas[1]][v].append(
                            str(np.percentile(np.array(tmp), 90)).split(".")[0])
                    else:
                        tmp = [[t[0]] for t in noflu.values_list(tipo_tiempo)]
                        new_dict_operativa[no][sub_operativas[1]][v] = [
                            str(np.percentile(np.array(tmp), 90)).split(".")[0]]

    no = 'OPERACION CON DNI'
    if reop.filter(nombre_operativa=no):
        for k, v in dict_operativas[no].items():
            noflu = reop.filter(nombre_flujo=k)
            if noflu:
                if k.split(' ')[0] == 'Retiro':
                    nob = dic_operativa_cuenta[1]
                else:
                    nob = dic_operativa_cuenta[0]

                if not nob in new_dict_operativa.keys():
                    new_dict_operativa[nob] = {}

                if not sub_operativas[0] in new_dict_operativa[nob].keys():
                    new_dict_operativa[nob][sub_operativas[0]] = {}

                if v in new_dict_operativa[nob][sub_operativas[0]].keys():
                    tmp = [[t[0]] for t in noflu.values_list(tipo_tiempo)]
                    new_dict_operativa[nob][sub_operativas[0]][v].append(
                        str(np.percentile(np.array(tmp), 90)).split(".")[0])
                else:
                    tmp = [[t[0]] for t in noflu.values_list(tipo_tiempo)]
                    new_dict_operativa[nob][sub_operativas[0]][v] = [
                        str(np.percentile(np.array(tmp), 90)).split(".")[0]]

    no = 'OPERACION CON LECTURA DE TARJETA'
    if reop.filter(nombre_operativa=no):
        for k, v in dict_operativas[no].items():
            noflu = reop.filter(nombre_flujo=k)
            if noflu:
                if k.split(' ')[0] == 'Retiro':
                    nob = dic_operativa_cuenta[1]
                else:
                    nob = dic_operativa_cuenta[0]

                if not nob in new_dict_operativa.keys():
                    new_dict_operativa[nob] = {}

                if not sub_operativas[2] in new_dict_operativa[nob].keys():
                    new_dict_operativa[nob][sub_operativas[2]] = {}

                if v in new_dict_operativa[nob][sub_operativas[2]].keys():
                    tmp = [[t[0]] for t in noflu.values_list(tipo_tiempo)]
                    new_dict_operativa[nob][sub_operativas[2]][v].append(
                        str(np.percentile(np.array(tmp), 90)).split(".")[0])
                else:
                    tmp = [[t[0]] for t in noflu.values_list(tipo_tiempo)]
                    new_dict_operativa[nob][sub_operativas[2]][v] = [
                        str(np.percentile(np.array(tmp), 90)).split(".")[0]]

    new_list = []
    for k, v in new_dict_operativa.items():
        subco = []
        for i, j in v.items():
            co = []
            for c in columns:
                if c in j:
                    df = pd.DataFrame(pd.to_timedelta(j[c])).mean()
                    co.append(str(df[0]).split('days ')[1].split('.')[0])
                else:
                    co.append('-')
            subco.append({
                'suboperativa': i,
                'columnas': co
            })
        new_list.append({
            'operativa': k,
            'suboperativas': subco
        })

    print(new_list)

    ofi = Oficinas.objects.filter(codigo_oficina=oficina)

    return render(request, 'home/Lista_tiempos_operativas.html',
                  {'oficina': ofi, 'columnas': columns, 'flujos': new_list,
                   'tipo': tipo_tiempo.replace('_', ' del ')})


def reporte_tiempo2(request, tipo_tiempo):
    columns = ['Normal', 'Tx Significativas', 'Tx Sig. Impresion',
               'Tx Sig. Autorización', 'Tx Sig. Autorización Impresion']
    dic_operativa_cuenta = [
        'DEPOSITO EN EFECTIVO',
        'RETIRO DE EFECTIVO',
        'RECAUDOS Y PAGO DE SERVICIOS'
    ]

    sub_operativas = ['DNI', 'CUENTA', 'TARJETA']

    new_dict_operativa = {}
    reop = ResumenOperation.objects.filter(estado=True)

    for no in dic_operativa_cuenta:
        if reop.filter(nombre_operativa=no):
            if no == 'DEPOSITO EN EFECTIVO':
                for k, v in dict_operativas['DEPÓSITO CON CHEQUES'].items():
                    noflu = reop.filter(nombre_flujo=k)
                    if noflu:
                        nob = 'DEPÓSITO CON CHEQUES'
                        if not nob in new_dict_operativa.keys():
                            new_dict_operativa[nob] = {}

                        if not sub_operativas[1] in new_dict_operativa[nob].keys():
                            new_dict_operativa[nob][sub_operativas[1]] = {}

                        if v in new_dict_operativa['DEPÓSITO CON CHEQUES'][sub_operativas[1]].keys():
                            tmp = [[t[0]] for t in noflu.values_list(tipo_tiempo)]
                            new_dict_operativa['DEPÓSITO CON CHEQUES'][sub_operativas[1]][v].append(
                                str(np.percentile(np.array(tmp), 90)).split(".")[0])
                        else:
                            tmp = [[t[0]] for t in noflu.values_list(tipo_tiempo)]
                            new_dict_operativa['DEPÓSITO CON CHEQUES'][sub_operativas[1]][v] = [
                                str(np.percentile(np.array(tmp), 90)).split(".")[0]]

            if not no in new_dict_operativa.keys():
                new_dict_operativa[no] = {}

            if not sub_operativas[1] in new_dict_operativa[no].keys():
                new_dict_operativa[no][sub_operativas[1]] = {}

            for k, v in dict_operativas[no].items():
                # print(k,' -> ', v)
                noflu = reop.filter(nombre_flujo=k)
                if noflu:
                    if v in new_dict_operativa[no][sub_operativas[1]].keys():
                        tmp = [[t[0]] for t in noflu.values_list(tipo_tiempo)]
                        new_dict_operativa[no][sub_operativas[1]][v].append(
                            str(np.percentile(np.array(tmp), 90)).split(".")[0])
                    else:
                        tmp = [[t[0]] for t in noflu.values_list(tipo_tiempo)]
                        new_dict_operativa[no][sub_operativas[1]][v] = [
                            str(np.percentile(np.array(tmp), 90)).split(".")[0]]

    no = 'OPERACION CON DNI'
    if reop.filter(nombre_operativa=no):
        for k, v in dict_operativas[no].items():
            noflu = reop.filter(nombre_flujo=k)
            if noflu:
                if k.split(' ')[0] == 'Retiro':
                    nob = dic_operativa_cuenta[1]
                else:
                    nob = dic_operativa_cuenta[0]

                if not nob in new_dict_operativa.keys():
                    new_dict_operativa[nob] = {}

                if not sub_operativas[0] in new_dict_operativa[nob].keys():
                    new_dict_operativa[nob][sub_operativas[0]] = {}

                if v in new_dict_operativa[nob][sub_operativas[0]].keys():
                    tmp = [[t[0]] for t in noflu.values_list(tipo_tiempo)]
                    new_dict_operativa[nob][sub_operativas[0]][v].append(
                        str(np.percentile(np.array(tmp), 90)).split(".")[0])
                else:
                    tmp = [[t[0]] for t in noflu.values_list(tipo_tiempo)]
                    new_dict_operativa[nob][sub_operativas[0]][v] = [
                        str(np.percentile(np.array(tmp), 90)).split(".")[0]]

    no = 'OPERACION CON LECTURA DE TARJETA'
    if reop.filter(nombre_operativa=no):
        for k, v in dict_operativas[no].items():
            noflu = reop.filter(nombre_flujo=k)
            if noflu:
                if k.split(' ')[0] == 'Retiro':
                    nob = dic_operativa_cuenta[1]
                else:
                    nob = dic_operativa_cuenta[0]

                if not nob in new_dict_operativa.keys():
                    new_dict_operativa[nob] = {}

                if not sub_operativas[2] in new_dict_operativa[nob].keys():
                    new_dict_operativa[nob][sub_operativas[2]] = {}

                if v in new_dict_operativa[nob][sub_operativas[2]].keys():
                    tmp = [[t[0]] for t in noflu.values_list(tipo_tiempo)]
                    new_dict_operativa[nob][sub_operativas[2]][v].append(
                        str(np.percentile(np.array(tmp), 90)).split(".")[0])
                else:
                    tmp = [[t[0]] for t in noflu.values_list(tipo_tiempo)]
                    new_dict_operativa[nob][sub_operativas[2]][v] = [
                        str(np.percentile(np.array(tmp), 90)).split(".")[0]]

    new_list = []
    for k, v in new_dict_operativa.items():
        subco = []
        for i, j in v.items():
            co = []
            for c in columns:
                if c in j:
                    df = pd.DataFrame(pd.to_timedelta(j[c])).mean()
                    co.append(str(df[0]).split('days ')[1].split('.')[0])
                else:
                    co.append('-')
            subco.append({
                'suboperativa': i,
                'columnas': co
            })
        new_list.append({
            'operativa': k,
            'suboperativas': subco
        })

    return render(request, 'home/reporte2.html',
                  {'columnas': columns, 'flujos': new_list,
                   'tipo': tipo_tiempo.replace('_', ' de ')})


def reporte_tiempo(request, tipo_tiempo):
    sub_operativas = ['DNI', 'CUENTA', 'TARJETA']
    oficin = Oficinas.objects.all()
    aaaa = {}
    for ofi in oficin:
        reop = ResumenOperation.objects.filter(estado=True, codigo_oficina=ofi.codigo_oficina)
        if reop:
            new_dict_operativa = {}
            for no in dic_operativa_cuenta:
                if reop.filter(nombre_operativa=no):
                    if no == 'DEPOSITO EN EFECTIVO':
                        for k, v in dict_operativas['DEPÓSITO CON CHEQUES'].items():
                            noflu = reop.filter(nombre_flujo=k)
                            if noflu:
                                nob = 'DEPÓSITO CON CHEQUES'
                                if not nob in new_dict_operativa.keys():
                                    new_dict_operativa[nob] = {}

                                if not sub_operativas[1] in new_dict_operativa[nob].keys():
                                    new_dict_operativa[nob][sub_operativas[1]] = {}

                                if v in new_dict_operativa['DEPÓSITO CON CHEQUES'][sub_operativas[1]].keys():
                                    tmp = [[t[0]] for t in noflu.values_list(tipo_tiempo)]
                                    new_dict_operativa['DEPÓSITO CON CHEQUES'][sub_operativas[1]][v].append(
                                        str(np.percentile(np.array(tmp), 90)).split(".")[0])
                                else:
                                    tmp = [[t[0]] for t in noflu.values_list(tipo_tiempo)]
                                    new_dict_operativa['DEPÓSITO CON CHEQUES'][sub_operativas[1]][v] = [
                                        str(np.percentile(np.array(tmp), 90)).split(".")[0]]

                    if not no in new_dict_operativa.keys():
                        new_dict_operativa[no] = {}

                    if not sub_operativas[1] in new_dict_operativa[no].keys():
                        new_dict_operativa[no][sub_operativas[1]] = {}

                    for k, v in dict_operativas[no].items():
                        # print(k,' -> ', v)
                        noflu = reop.filter(nombre_flujo=k)
                        if noflu:
                            if v in new_dict_operativa[no][sub_operativas[1]].keys():
                                tmp = [[t[0]] for t in noflu.values_list(tipo_tiempo)]
                                new_dict_operativa[no][sub_operativas[1]][v].append(
                                    str(np.percentile(np.array(tmp), 90)).split(".")[0])
                            else:
                                tmp = [[t[0]] for t in noflu.values_list(tipo_tiempo)]
                                new_dict_operativa[no][sub_operativas[1]][v] = [
                                    str(np.percentile(np.array(tmp), 90)).split(".")[0]]

            no = 'OPERACION CON DNI'
            if reop.filter(nombre_operativa=no):
                for k, v in dict_operativas[no].items():
                    noflu = reop.filter(nombre_flujo=k)
                    if noflu:
                        if k.split(' ')[0] == 'Retiro':
                            nob = dic_operativa_cuenta[1]
                        else:
                            nob = dic_operativa_cuenta[0]

                        if not nob in new_dict_operativa.keys():
                            new_dict_operativa[nob] = {}

                        if not sub_operativas[0] in new_dict_operativa[nob].keys():
                            new_dict_operativa[nob][sub_operativas[0]] = {}

                        if v in new_dict_operativa[nob][sub_operativas[0]].keys():
                            tmp = [[t[0]] for t in noflu.values_list(tipo_tiempo)]
                            new_dict_operativa[nob][sub_operativas[0]][v].append(
                                str(np.percentile(np.array(tmp), 90)).split(".")[0])
                        else:
                            tmp = [[t[0]] for t in noflu.values_list(tipo_tiempo)]
                            new_dict_operativa[nob][sub_operativas[0]][v] = [
                                str(np.percentile(np.array(tmp), 90)).split(".")[0]]

            no = 'OPERACION CON LECTURA DE TARJETA'
            if reop.filter(nombre_operativa=no):
                for k, v in dict_operativas[no].items():
                    noflu = reop.filter(nombre_flujo=k)
                    if noflu:
                        if k.split(' ')[0] == 'Retiro':
                            nob = dic_operativa_cuenta[1]
                        else:
                            nob = dic_operativa_cuenta[0]

                        if not nob in new_dict_operativa.keys():
                            new_dict_operativa[nob] = {}

                        if not sub_operativas[2] in new_dict_operativa[nob].keys():
                            new_dict_operativa[nob][sub_operativas[2]] = {}

                        if v in new_dict_operativa[nob][sub_operativas[2]].keys():
                            tmp = [[t[0]] for t in noflu.values_list(tipo_tiempo)]
                            new_dict_operativa[nob][sub_operativas[2]][v].append(
                                str(np.percentile(np.array(tmp), 90)).split(".")[0])
                        else:
                            tmp = [[t[0]] for t in noflu.values_list(tipo_tiempo)]
                            new_dict_operativa[nob][sub_operativas[2]][v] = [
                                str(np.percentile(np.array(tmp), 90)).split(".")[0]]

            aaaa[ofi.oficina] = new_dict_operativa

    new_list_p = []
    for a, b in aaaa.items():
        new_list = []
        for k, v in b.items():
            subco = []
            for i, j in v.items():
                co = []
                for c in columns:
                    if c in j:
                        df = pd.DataFrame(pd.to_timedelta(j[c])).mean()
                        co.append(str(df[0]).split('days ')[1].split('.')[0])
                    else:
                        co.append('-')
                subco.append({
                    'suboperativa': i,
                    'columnas': co
                })
            new_list.append({
                'operativa': k,
                'suboperativas': subco
            })
        new_list_p.append({
            'oficina': a,
            'suboperativas': new_list
        })
    # print(new_list_p)
    return render(request, 'home/reporte_operativas.html',
                  {'columnas': columns, 'flujos': new_list_p,
                   'tipo': tipo_tiempo.replace('_', ' de ')})


'''def get_list_subflujo_oficina(request, oficina):
    # Filtro
    estado = True
    if request.GET.get('estado'):
        estado = request.GET.get('estado')

    set_list_nombre_flujos = ResumenOperation.objects.values('nombre_flujo').distinct().filter(estado=estado,
                                                                                               codigo_oficina=oficina)

    # Filtros
    codigo_operativa = request.GET.get('codigo_operativa', '')
    fecha = request.GET.get('fecha', '')
    if codigo_operativa != '':
        set_list_nombre_flujos = set_list_nombre_flujos.filter(codigo_operativa=codigo_operativa,
                                                               codigo_oficina=oficina)
    if fecha != '':
        set_list_nombre_flujos = set_list_nombre_flujos.filter(fecha=fecha, codigo_oficina=oficina)

    # Datos
    lista_x_flujos_resumen_operativas = []
    nombres = [n.get('nombre_flujo') for n in set_list_nombre_flujos]
    nombres = sorted(nombres)
    for i in range(0, len(nombres)):
        datos_x_flujo = set_list_nombre_flujos.filter(nombre_flujo=nombres[i],
                                                      codigo_oficina=oficina).values().order_by('tiempo_operativa')
        # Calcular
        response_estadistico = Utilitarios.getDatosxFlujo(datos_flujo=datos_x_flujo,
                                                          self=['tiempo_operativa', 'tiempo_sistema',
                                                                'tiempo_disponible', 'tiempo_escenario'])
        # Añadir
        lista_x_flujos_resumen_operativas.append({
            'indice': i + 1,
            'nombre_flujo': nombres[i].replace('/', '_'),
            'datos': datos_x_flujo,
            'datos_calculados': response_estadistico,
            'percent': 0
        })

    total_flujo = sum([len(li['datos']) for li in lista_x_flujos_resumen_operativas])
    print(total_flujo)
    for j in lista_x_flujos_resumen_operativas:
        j['percent'] = round(len(j['datos']) / total_flujo * 100)

    # print(lista_x_flujos_resumen_operativas[0])
    # print(sorted(lista_x_flujos_resumen_operativas[0].iteritems(), key=lambda k,v: v))

    return render(request, 'home/resumen_operativas.html',
                  {'resumen_operativas_xflujos': lista_x_flujos_resumen_operativas, 'rango': range(4)})
'''


def get_list_subflujo_resumen_operativas(request):
    # Filtro
    resumenOpe = ResumenOperation.objects
    estado = request.GET.get('estado', 'True')
    fecha = request.GET.get('fecha')

    set_list_nombre_flujos = resumenOpe.values('nombre_flujo').distinct().filter(estado=estado)
    oficina = request.GET.get('oficina')
    if oficina:
        fechas = resumenOpe.values('fecha').distinct().filter(codigo_oficina=oficina).order_by('-fecha')
        if not fecha:
            fecha = fechas[0]['fecha']
        set_list_nombre_flujos = set_list_nombre_flujos.filter(codigo_oficina=oficina, fecha=fecha)
    else:
        fechas = resumenOpe.values('fecha').distinct().order_by('-fecha')
        if not fecha:
            fecha = fechas[0]['fecha']
        set_list_nombre_flujos = set_list_nombre_flujos.filter(fecha=fecha)

    if isinstance(fecha, str):
        fecha = datetime.strptime(fecha, '%Y-%m-%d')

    # Filtros
    codigo_operativa = request.GET.get('codigo_operativa', '')
    if codigo_operativa != '':
        set_list_nombre_flujos = set_list_nombre_flujos.filter(codigo_operativa=codigo_operativa)

    # Datos
    #print([{'flu':n.get('nombre_flujo'), 'ofi':n.get('codigo_oficina'),'pue':n.get('puesto')} for n in set_list_nombre_flujos])
    lista_x_flujos_resumen_operativas = []
    nombres = [n.get('nombre_flujo') for n in set_list_nombre_flujos]
    nombres = sorted(nombres)
    for i in range(0, len(nombres)):
        datos_x_flujo = set_list_nombre_flujos.filter(nombre_flujo=nombres[i]).values().order_by('tiempo_operativa')
        # Calcular
        response_estadistico = Utilitarios.getDatosxFlujo(datos_flujo=datos_x_flujo,
                                                          self=['tiempo_operativa', 'tiempo_sistema',
                                                                'tiempo_disponible', 'tiempo_usuario'])
        # Añadir
        lista_x_flujos_resumen_operativas.append({
            'indice': i + 1,
            'nombre_flujo': nombres[i].replace('/', '_'),
            'datos': datos_x_flujo,
            'datos_calculados': response_estadistico,
            'percent': 0
        })

    total_flujo = sum([len(li['datos']) for li in lista_x_flujos_resumen_operativas])
    print(total_flujo)
    for j in lista_x_flujos_resumen_operativas:
        j['percent'] = round(len(j['datos']) / total_flujo * 100)

    # print(lista_x_flujos_resumen_operativas[0])
    # print(sorted(lista_x_flujos_resumen_operativas[0].iteritems(), key=lambda k,v: v))
    ofi = Oficinas.objects.filter(codigo_oficina=oficina)

    return render(request, 'home/resumen_operativas.html',
                  {'resumen_operativas_xflujos': lista_x_flujos_resumen_operativas, 'rango': range(4),
                   'oficina': ofi, 'fecha': fecha, 'fechas': fechas
                   })


def get_subflujo_resumen_operativas(request, subflujo):
    subflujo = subflujo.replace('_', '/')

    # Filtro
    estado = True
    if request.GET.get('estado'):
        estado = request.GET.get('estado')

    resumen_operativas_flujo = ResumenOperation.objects.filter(estado=estado, nombre_flujo=subflujo).order_by('fecha',
                                                                                                              'hora_inicio')

    data_general = {'subflujo': subflujo}

    # Filtros
    codigo_operativa = request.GET.get('codigo_operativa', '')

    oficina = request.GET.get('oficina', '')
    if oficina:
        resumen_operativas_flujo = resumen_operativas_flujo.filter(codigo_oficina=oficina)
    else:
        resumen_operativas_flujo = resumen_operativas_flujo

    fecha = request.GET.get('fecha', '')
    if codigo_operativa != '':
        resumen_operativas_flujo = resumen_operativas_flujo.filter(codigo_operativa=codigo_operativa).order_by('fecha',
                                                                                                               'hora_inicio')
        data_general['codigo_operativa'] = codigo_operativa
    if fecha != '':
        resumen_operativas_flujo = resumen_operativas_flujo.filter(fecha=fecha).order_by('fecha', 'hora_inicio')
        data_general['fecha'] = fecha
    resumen_operativas_flujo = resumen_operativas_flujo.order_by('fecha', 'hora_inicio')

    rango = range(1, len(resumen_operativas_flujo) + 1)
    data_general['rango'] = len(resumen_operativas_flujo)

    # Datos
    return render(request, 'home/resumen_operativas_list.html',
                  {'resumen_operativas_flujo': resumen_operativas_flujo, 'rango': rango, 'data_general': data_general})


def get_detalle_operativas(request, identificador):
    fecha = request.GET.get('fecha', '')
    detalle_operativa = DetalleOperation.objects.filter(identificador=identificador).order_by('id')
    if fecha:
        detalle_operativa = detalle_operativa.filter(fecha=fecha)

    return render(request, 'home/detalle_operativa_detail.html', {'detalle_operativa': detalle_operativa})


def get_list_detalle_operativas(request):
    # Filtros
    estado = request.GET.get('estado', 'True')
    detalle_operativa = DetalleOperation.objects.distinct('codigo_ventana_tx').order_by('codigo_ventana_tx').filter(
        estado=estado)

    codigo_operativa = request.GET.get('codigo_operativa', None)
    if codigo_operativa:
        detalle_operativa = detalle_operativa.filter(codigo_operativa=codigo_operativa)

    rango = range(1, len(detalle_operativa) + 1)

    return render(request, 'home/detalle_operativa_list.html', {'detalle_operativa': detalle_operativa, 'rango': rango})
