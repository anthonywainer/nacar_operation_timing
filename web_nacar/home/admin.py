from django.contrib import admin
from .models import ResumenOperation
from .models import DetalleOperation
from .models import Test

# Register your models here.
admin.site.register(ResumenOperation)
admin.site.register(DetalleOperation)
admin.site.register(Test)