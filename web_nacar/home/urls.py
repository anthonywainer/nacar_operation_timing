from django.urls import path
from . import views

urlpatterns = [
    path('', views.index_reportes, name='index_reportes'),

    path('dashboard/', views.dashboard, name='dashboard'),
    path('dashboard/indicador/', views.dashboard_indicador, name='dashboard_indicador'),

    path('oficina/', views.get_list_subflujo_resumen_operativas, name='get_list_subflujo_resumen_operativas'),

    #reporte1 -> tiempos/oficinas
    path('reporte/', views.reporte),
    path('reporte/<str:tipo_tiempo>/', views.reporte_tiempo),

    #reporte2 -> tiempos/
    path('reporte2/', views.reporte2),
    path('reporte2/<str:tipo_tiempo>/', views.reporte_tiempo2),



    path('oficina/<int:oficina>', views.get_list_subflujo_oficina),

    path('oficina/<int:oficina>/<str:tipo_tiempo>/', views.get_list_oficina_tiempo),

    #Resumen
    path('resumen_operativas/', views.get_list_resumen_operativas, name='get_list_resumen_operativas'),
    ##Subflujo



    path('resumen_operativas/subflujo/<str:subflujo>', views.get_subflujo_resumen_operativas, name='get_subflujo_resumen_operativas'),
    #Detalle
    path('detalle_operativas/', views.get_list_detalle_operativas, name='get_list_detalle_operativas'),
    path('detalle_operativas/<str:identificador>', views.get_detalle_operativas, name='get_detalle_operativas'),

]