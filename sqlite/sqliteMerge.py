#!/usr/bin/python

import sys, sqlite3


class sqliteMerge(object):

    def __init__(self, parent=None):
        super(sqliteMerge, self).__init__()

        self.db_a = None
        self.db_b = None
        self.db_base = None

    def loadTables(self, file_a, file_b):
        self.db_a = sqlite3.connect(file_a)
        self.db_a = sqlite3.connect(file_b)

        cursor_a = self.db_a.cursor()
        cursor_a.execute("SELECT name FROM sqlite_master WHERE type='table';")

        table_counter = 0
        print("SQL Tables available: \n===================================================\n")
        for table_item in cursor_a.fetchall():
            current_table = table_item[0]
            table_counter += 1
            print("-> " + current_table)
        print("\n===================================================\n")

        if table_counter == 1:
            table_to_merge = current_table
        else:
            table_to_merge = input("Table to Merge: ")

        return table_to_merge

    def merge(self, table_name):
        file_name_a = 'timelog1.sqlite3'  # input("File Name A:")
        file_name_b = 'timelog2.sqlite3'  # input("File Name B:")
        file_name_base = 'base.sqlite3'  # input("File Name B:")
        self.db_a = sqlite3.connect(file_name_a)
        self.db_b = sqlite3.connect(file_name_b)
        self.db_base = sqlite3.connect(file_name_base)
        cursor_a = self.db_a.cursor()
        cursor_b = self.db_b.cursor()

        try:
            nc = ''
            cols_q = cursor_a.execute("SELECT name FROM PRAGMA_TABLE_INFO('" + table_name + "') ")
            cv = list(cols_q)
            for n in cv:
                nc += str(n[0]) + ', '

            cols = nc[:-2]

            # cursor_a.execute("CREATE TABLE IF NOT EXISTS " + new_table_name + " AS SELECT * FROM " + table_name)
            query = 'INSERT INTO ' + table_name + ' (' + cols + ') VALUES ( ?' + (len(cv)-1)*', ?' + ')'
            self.db_base.executemany(query, list(cursor_a.execute("SELECT " + cols + " FROM " + table_name))
                                     + list(cursor_b.execute("SELECT " + cols + " FROM " + table_name)))
            self.db_base.commit()

            # cursor_a.execute("DROP TABLE IF EXISTS " + table_name);
            # cursor_a.execute("ALTER TABLE " + new_table_name + " RENAME TO " + table_name);
            print("\n\nMerge Successful!\n")

        except (Exception, sqlite3.DatabaseError) as error:
            print("ERROR!: Merge Failed", error)
            # cursor_a.execute("DROP TABLE IF EXISTS " + new_table_name);

        return

    def main(self):

        # table_name = self.loadTables(file_name_a, file_name_b)
        for t in ['operation_timing_detalle', 'operation_timing_resumen']:
            self.merge(t)

        self.db_a.close()
        self.db_b.close()
        self.db_base.close()
        return


if __name__ == '__main__':
    app = sqliteMerge()
    app.main()
