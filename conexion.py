import sqlite3 as db_prod
import psycopg2 as db_test


def initConn(datos, cad):
    if not datos.get('port'):
        datos['port'] = 5432

    if cad == '_prod':
        db = db_prod
        conn = db.connect(database='timelog.sqlite3')
    else:
        db = db_test
        conn = db.connect(database=datos.get('database') + '_prod',
                          user=datos.get('user'),
                          password=datos.get('password'),
                          host=datos.get('localhost'),
                          port=datos.get('port')
                          )

    return conn


def insertResumen_Operativas(conn, datos, tipo, cad):
    # Query
    query = 'INSERT INTO operation_timing_resumen (estado, identificador, fecha, puesto,' \
            'codigo_oficina, iter_oficina, nombre_operativa, iter_puesto, hora_inicio, hora_fin,' \
            'codigo_escenario, codigo_operativa, tiempo_operativa,' \
            'tiempo_sistema, tiempo_disponible, tiempo_usuario, tiempo_disponible_neto,' \
            'tiempo_disponible_real, tiempo_disponible_real_a, tiempo_disponible_real_b,' \
            'numero_tramos, iter_flujo_puesto, iter_flujo_oficina, nombre_flujo, ' \
            'hora_escenario, terminal, usuario, hora_ini_escenario, hora_ini_operativa, '\
            'hora_fin_escenario, hora_fin_operativa, hora_operativa ' \
            ') '

    if cad == '_prod':
        db = db_prod
        conn = db.connect(database='timelog.sqlite3')
        tipo = '1' if eval(tipo) else '0'
        query += 'VALUES (' + tipo + 31 * ', ?' + ')'

    else:
        db = db_test
        query += 'VALUES (' + tipo + 31 * ', %s' + ')'

    try:

        cur = conn.cursor()
        cur.executemany(query, datos)
        conn.commit()
        cur.close()
        print("INFO: Se insertaron", len(datos), "(", tipo[0], ") registros a la BD resumen_operativas.")

    except (Exception, db.DatabaseError) as error:
        print(len(datos[0]))
        print("ERROR:", error)


def insertDetalle_Operativas(conn, datos, tipo, cad):
    # Query
    query = 'INSERT INTO operation_timing_detalle (estado, identificador, codigo_oficina,' \
            'fecha, iter_oficina, puesto, iter_puesto, codigo_escenario, nombre_operativa, codigo_operativa,' \
            'tiempo_operativa, tiempo_parcial, hora_inicio, hora_fin, hora_palabra_reservada, ' \
            'codigo_ventana_tx,detalle_ventana_tx, iter_tramo, numero_tramos, nombre_flujo, ' \
            'iter_flujo_puesto, iter_flujo_oficina' \
            ') '
            #'hora_escenario, terminal, usuario, hora_ini_escenario, hora_ini_operativa'\

    if cad == '_prod':
        db = db_prod
        conn = db.connect(database='timelog.sqlite3')
        tipo = '1' if tipo else '0'
        query += 'VALUES (' + tipo + 21 * ', ?' + ')'

    else:
        db = db_test
        query += 'VALUES (' + tipo + 21 * ', %s' + ')'

    try:
        cur = conn.cursor()
        cur.executemany(query, datos)
        conn.commit()
        cur.close()
        print("INFO: Se insertaron", len(datos), "(", tipo[0], ") registros a la BD detalle_operativas.")

    except (Exception, db.DatabaseError) as error:
        print("ERROR:", error)
