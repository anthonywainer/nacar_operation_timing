# coding=utf-8
import logging
from os import listdir
import io
import os
from queue import Queue
from threading import Thread

import pandas as pd
import numpy as np

'''
    Inicializar configuracion
'''


def iniConfig(file='config'):
    f = open(file, 'r')
    m = ""
    for a in f:
        if len(a) > 0 and a[0] != '#':
            m += a
    f.close()
    contenido = m.split(';')
    tmp = [c.replace(' ', '').replace('\t', '').replace('\n', '').replace('{', '').replace('}', '') for c in contenido]

    datos = {}
    for t in tmp:
        m = t.split('=')
        if len(m) == 2:
            datos[m[0]] = m[1]
    #
    if len(datos) >= 2:
        if datos.get('analizar'):
            # Directorio de salida
            if datos.get('output'):
                if (datos['output'] != "") and (not os.path.isdir(datos['output'])):
                    print("WARNING: No existe el directorio de salida indicado. Se utilizará el directorio por defecto")
                    datos['output'] = ""
                else:
                    if len(datos['output']) > 1 and datos['output'][-1] != '/':
                        datos['output'] = datos['output'] + '/'
            else:
                print("INFO: Se utilizará el directorio de salida por defecto")
                datos['output'] = ""
            # Patrones a analizar
            analizar = [[a.split(':')[0], a.split(':')[1]] for a in datos['analizar'].split(',')]
            datos['analizar'] = analizar

            especiales = []
            # Patrones especiales
            if datos.get('especiales'):
                especiales = [b for b in datos['especiales'].split(',')]
                datos['especiales'] = especiales

            # Datos de conexion a BD
            datos_conexion = getDatafromConexion()

            return {
                'output': datos['output'],
                'lista_especiales': datos['especiales'],
                'datos_conexion': datos_conexion
            }
    # Error
    print("ERROR: Archivo de configuracion incorrecto (config)" +
          "\nVariables a ingresar: analizar, carpeta, fecha, output*")
    exit(1)


'''
    Obtencion de archivos
'''


def getDatafromConexion(file='conexion'):
    f = open(file, 'r')
    m = ""
    for a in f:
        if len(a) > 0 and a[0] != '#':
            m += a
    f.close()
    contenido = m.split('\n')

    datos = {}
    for t in contenido:
        m = t.split('=')
        if len(m) == 2:
            datos[m[0]] = m[1]

    if len(datos) == 5 or (len(datos) == 4 and not datos.get('puerto')):
        return datos
    else:
        print("Error en el archivo de conexion a base de datos")
        exit(1)


def getNomFilesExistentes(carpeta, tmp_nomfiles):
    files_existentes = []

    for file in tmp_nomfiles:
        if os.path.isfile(carpeta + file):
            files_existentes.append(file)
    return files_existentes


# En desuso
def getFoldersExistentes(carpeta, tmp_nom_files):
    folders_existentes = []
    for file in tmp_nom_files:
        if os.path.isdir(carpeta + file):
            folders_existentes.append(carpeta + file + "/")
    return folders_existentes


def getNomFiles(carpeta):
    data_in = sorted(listdir(carpeta))
    lista_nomfiles = []
    lista_sina = []

    for l in data_in:
        if l.count('_') >= 3 and l.find('ERROR') == -1:
            if l.count('_') == 3:
                lista_sina.append(l)
            elif l.count('_') == 4:
                if l[-5] != '0':
                    lista_sina.append(l[:-6] + l[-4:])
                else:
                    lista_sina.append(l[:-7] + l[-4:])

    tmp_lista = sorted(list(set(lista_sina)))
    for l in tmp_lista:
        tmp_nomfiles = [l] + [l[:-4] + "_" + str(i) + l[-4:] for i in range(1, 11)]
        nomfiles = getNomFilesExistentes(carpeta, tmp_nomfiles)
        lista_nomfiles.append(nomfiles)

    total_nomfiles = []
    for a in lista_nomfiles:
        total_nomfiles.append([carpeta + b for b in a])

    return total_nomfiles


'''
    Extracción y filtro de datos (limpieza)
'''


def getFilasInTuplas(nomfile, li):
    print("INFO: Datos obtenidos de ", nomfile)

    f = io.open(nomfile, 'r', encoding='latin-1')
    in_data = []

    for linea in f:
        t = tuple(linea.split(' ', 2))
        if "TERMINAL" in t[0]:
            TERMINAL = t[2].strip()
            ldata = len(in_data)
            USUARIO = in_data[ldata - 1][2].strip()
            PUESTO = str(in_data[ldata - 4][2]).strip()
            OFICINA = in_data[ldata - 5][2].strip()[-4:]
            try:
                li[OFICINA[1:]]["p" + PUESTO] = {'terminal': TERMINAL, 'usuario': USUARIO}
            except KeyError:
                pass

            continue

        if len(t) >= 3:
            in_data.append(t)
    f.close()
    return in_data, li


def getMatrizDatosHoy(fecha_eval, lista_nomfiles):
    # print("INFO: ANALIZANDO TRAZAS DE ", lista_nomfiles[0])

    final_data = pd.DataFrame({
        '1. Fecha': [],
        '2. Hora': [],
        '6. Detalle': []
    })

    li_ofi = {}

    for nomfile in lista_nomfiles:
        num_puesto = getPuestofromNom(nomfile)
        cod_oficina = getCodOficina(nomfile)
        cod_oficina = cod_oficina[2:5]
        if not cod_oficina in li_ofi.keys():
            li_ofi[cod_oficina] = {}
            if not num_puesto in li_ofi[cod_oficina].keys():
                li_ofi[cod_oficina][num_puesto] = {}

        in_data, li_ofi = getFilasInTuplas(nomfile, li_ofi)
        inputs = np.array(in_data)
        fecha = inputs[:, 0]
        hora = inputs[:, 1]
        detalle = inputs[:, 2]

        data = pd.DataFrame({
            '1. Fecha': fecha,
            '2. Hora': hora,
            '6. Detalle': detalle
        })

        # Get dataframe de fecha
        data_filter = data[data['1. Fecha'] == fecha_eval]

        final_data = final_data.append(data_filter, sort=True)

    # Reordenando por tiempo e index
    data = final_data.reset_index()
    data = data.sort_values(by=['2. Hora', 'index'])
    del data['index']

    return data.reset_index(drop=True), li_ofi


'''
    ETC
'''


def getPuestofromNom(nombre):
    if nombre.count('_') == 3:
        return nombre[-10:-7]
    else:
        return nombre[-12:-9]


def getCodOficina(nombre):
    tmp = nombre.split('traza')
    if len(tmp) == 2 and tmp[1][0] == '_':
        return tmp[1][1:].split('_')[0]
    else:
        return 'None'


'''
    Generacion de archivos txt
'''


def genFile(nom_fdetalle, dato, ext):
    # Archivo de texto
    f = open(nom_fdetalle + '.' + ext, 'a')
    print("INFO: Almacenando resultados en archivo ", nom_fdetalle, '.txt')
    f.write(dato + '\n')
    f.close()


def formatCSV(matriz):
    if len(matriz) == 0:
        return ""

    result = ""
    for registro in matriz:
        result += ','.join([str(a) for a in registro])
        result += '\n'

    return result[:-1]
